/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50618
Source Host           : localhost:3306
Source Database       : my-erp

Target Server Type    : MYSQL
Target Server Version : 50618
File Encoding         : 65001

Date: 2018-02-11 00:10:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `databasechangelog`;
CREATE TABLE `databasechangelog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `databasechangeloglock`;
CREATE TABLE `databasechangeloglock` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of databasechangeloglock
-- ----------------------------
INSERT INTO `databasechangeloglock` VALUES ('1', '\0', null, null);

-- ----------------------------
-- Table structure for jsh_account
-- ----------------------------
DROP TABLE IF EXISTS `jsh_account`;
CREATE TABLE `jsh_account` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `SerialNo` varchar(50) DEFAULT NULL COMMENT '编号',
  `InitialAmount` double DEFAULT NULL COMMENT '期初金额',
  `CurrentAmount` double DEFAULT NULL COMMENT '当前余额',
  `Remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `IsDefault` bit(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jsh_accounthead
-- ----------------------------
DROP TABLE IF EXISTS `jsh_accounthead`;
CREATE TABLE `jsh_accounthead` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Type` varchar(50) DEFAULT NULL COMMENT '类型(支出/收入/收款/付款/转账)',
  `OrganId` bigint(20) DEFAULT NULL COMMENT '单位Id(收款/付款单位)',
  `HandsPersonId` bigint(20) DEFAULT NULL COMMENT '经手人Id',
  `ChangeAmount` double DEFAULT NULL COMMENT '变动金额(优惠/收款/付款/实付)',
  `TotalPrice` double DEFAULT NULL COMMENT '合计金额',
  `AccountId` bigint(20) DEFAULT NULL COMMENT '账户(收款/付款)',
  `BillNo` varchar(50) DEFAULT NULL COMMENT '单据编号',
  `BillTime` datetime DEFAULT NULL COMMENT '单据日期',
  `Remark` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Id`),
  KEY `FK9F4C0D8DB610FC06` (`OrganId`),
  KEY `FK9F4C0D8DAAE50527` (`AccountId`),
  KEY `FK9F4C0D8DC4170B37` (`HandsPersonId`),
  CONSTRAINT `FK9F4C0D8DAAE50527` FOREIGN KEY (`AccountId`) REFERENCES `jsh_account` (`Id`),
  CONSTRAINT `FK9F4C0D8DB610FC06` FOREIGN KEY (`OrganId`) REFERENCES `jsh_supplier` (`id`),
  CONSTRAINT `FK9F4C0D8DC4170B37` FOREIGN KEY (`HandsPersonId`) REFERENCES `jsh_person` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jsh_accountitem
-- ----------------------------
DROP TABLE IF EXISTS `jsh_accountitem`;
CREATE TABLE `jsh_accountitem` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `HeaderId` bigint(20) NOT NULL COMMENT '表头Id',
  `AccountId` bigint(20) DEFAULT NULL COMMENT '账户Id',
  `InOutItemId` bigint(20) DEFAULT NULL COMMENT '收支项目Id',
  `EachAmount` double DEFAULT NULL COMMENT '单项金额',
  `Remark` varchar(100) DEFAULT NULL COMMENT '单据备注',
  PRIMARY KEY (`Id`),
  KEY `FK9F4CBAC0AAE50527` (`AccountId`),
  KEY `FK9F4CBAC0C5FE6007` (`HeaderId`),
  KEY `FK9F4CBAC0D203EDC5` (`InOutItemId`),
  CONSTRAINT `FK9F4CBAC0AAE50527` FOREIGN KEY (`AccountId`) REFERENCES `jsh_account` (`Id`),
  CONSTRAINT `FK9F4CBAC0C5FE6007` FOREIGN KEY (`HeaderId`) REFERENCES `jsh_accounthead` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK9F4CBAC0D203EDC5` FOREIGN KEY (`InOutItemId`) REFERENCES `jsh_inoutitem` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jsh_app
-- ----------------------------
DROP TABLE IF EXISTS `jsh_app`;
CREATE TABLE `jsh_app` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Number` varchar(50) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Type` varchar(50) DEFAULT NULL,
  `Icon` varchar(50) DEFAULT NULL,
  `URL` varchar(50) DEFAULT NULL,
  `Width` varchar(50) DEFAULT NULL,
  `Height` varchar(50) DEFAULT NULL,
  `ReSize` bit(1) DEFAULT NULL,
  `OpenMax` bit(1) DEFAULT NULL,
  `Flash` bit(1) DEFAULT NULL,
  `ZL` varchar(50) DEFAULT NULL,
  `Sort` varchar(50) DEFAULT NULL,
  `Remark` varchar(200) DEFAULT NULL,
  `Enabled` bit(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jsh_app
-- ----------------------------
INSERT INTO `jsh_app` VALUES ('3', '00', '系统管理', 'app', '0000000004.png', '', '1240', '600', '', '\0', '\0', 'desk', '198', '', '');
INSERT INTO `jsh_app` VALUES ('6', '', '个人信息', 'app', '0000000005.png', '../user/password.jsp', '600', '400', '\0', '\0', '\0', 'dock', '200', '', '');
INSERT INTO `jsh_app` VALUES ('7', '01', '基础数据', 'app', '0000000006.png', '', '1240', '600', '', '\0', '\0', 'desk', '120', '', '');
INSERT INTO `jsh_app` VALUES ('8', '02', '组装拆卸', 'app', '0000000007.png', '', '1350', '630', '', '\0', '', 'desk', '030', '', '');
INSERT INTO `jsh_app` VALUES ('22', '03', '报表查询', 'app', '0000000022.png', '', '1240', '600', '', '\0', '\0', 'desk', '115', '', '');
INSERT INTO `jsh_app` VALUES ('23', '04', '零售管理', 'app', 'resizeApi.png', '', '1350', '630', '', '\0', '', 'desk', '025', '', '');
INSERT INTO `jsh_app` VALUES ('24', '05', '入库管理', 'app', 'buy.png', '', '1350', '630', '', '\0', '', 'desk', '027', '', '');
INSERT INTO `jsh_app` VALUES ('25', '06', '出库管理', 'app', 'sale.png', '', '1350', '630', '', '\0', '', 'desk', '028', '', '');
INSERT INTO `jsh_app` VALUES ('26', '07', '财务管理', 'app', 'money.png', '', '1350', '630', '', '\0', '\0', 'desk', '035', '', '');

-- ----------------------------
-- Table structure for jsh_asset
-- ----------------------------
DROP TABLE IF EXISTS `jsh_asset`;
CREATE TABLE `jsh_asset` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assetnameID` bigint(20) NOT NULL,
  `location` varchar(255) DEFAULT NULL COMMENT '位置',
  `labels` varchar(255) DEFAULT NULL COMMENT '标签：以空格为分隔符',
  `status` smallint(6) DEFAULT NULL COMMENT '资产的状态：0==在库，1==在用，2==消费',
  `userID` bigint(20) DEFAULT NULL,
  `price` double DEFAULT NULL COMMENT '购买价格',
  `purchasedate` datetime DEFAULT NULL COMMENT '购买日期',
  `periodofvalidity` datetime DEFAULT NULL COMMENT '有效日期',
  `warrantydate` datetime DEFAULT NULL COMMENT '保修日期',
  `assetnum` varchar(255) DEFAULT NULL COMMENT '资产编号',
  `serialnum` varchar(255) DEFAULT NULL COMMENT '资产序列号',
  `supplier` bigint(20) NOT NULL,
  `description` longtext COMMENT '描述信息',
  `addMonth` longtext COMMENT '资产添加时间，统计报表使用',
  `createtime` datetime DEFAULT NULL,
  `creator` bigint(20) DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  `updator` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK353690ED9B6CB285` (`assetnameID`),
  KEY `FK353690EDAD45B659` (`creator`),
  KEY `FK353690ED27D23FE4` (`supplier`),
  KEY `FK353690ED61FE182C` (`updator`),
  KEY `FK353690ED3E226853` (`userID`),
  CONSTRAINT `FK353690ED27D23FE4` FOREIGN KEY (`supplier`) REFERENCES `jsh_supplier` (`id`),
  CONSTRAINT `FK353690ED3E226853` FOREIGN KEY (`userID`) REFERENCES `jsh_user` (`id`),
  CONSTRAINT `FK353690ED61FE182C` FOREIGN KEY (`updator`) REFERENCES `jsh_user` (`id`),
  CONSTRAINT `FK353690ED9B6CB285` FOREIGN KEY (`assetnameID`) REFERENCES `jsh_assetname` (`id`),
  CONSTRAINT `FK353690EDAD45B659` FOREIGN KEY (`creator`) REFERENCES `jsh_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jsh_assetcategory
-- ----------------------------
DROP TABLE IF EXISTS `jsh_assetcategory`;
CREATE TABLE `jsh_assetcategory` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assetname` varchar(255) NOT NULL COMMENT '资产类型名称',
  `isystem` tinyint(4) NOT NULL COMMENT '是否系统自带 0==系统 1==非系统',
  `description` varchar(500) DEFAULT NULL COMMENT '描述信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
-- ----------------------------

-- ----------------------------
-- Table structure for jsh_assetname
-- ----------------------------
DROP TABLE IF EXISTS `jsh_assetname`;
CREATE TABLE `jsh_assetname` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assetname` varchar(255) NOT NULL COMMENT '资产名称',
  `assetcategoryID` bigint(20) NOT NULL,
  `isystem` smallint(6) NOT NULL COMMENT '是否系统自带 0==系统 1==非系统',
  `description` longtext COMMENT '描述信息',
  `isconsumables` smallint(6) DEFAULT NULL COMMENT '是否为耗材 0==否 1==是 耗材状态只能是消费',
  PRIMARY KEY (`id`),
  KEY `FKA4ADCCF866BC8AD3` (`assetcategoryID`),
  CONSTRAINT `FKA4ADCCF866BC8AD3` FOREIGN KEY (`assetcategoryID`) REFERENCES `jsh_assetcategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jsh_depot
-- ----------------------------
DROP TABLE IF EXISTS `jsh_depot`;
CREATE TABLE `jsh_depot` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(20) DEFAULT NULL COMMENT '仓库名称',
  `address` varchar(50) DEFAULT NULL COMMENT '仓库地址',
  `warehousing` double DEFAULT NULL COMMENT '仓储费',
  `truckage` double DEFAULT NULL COMMENT '搬运费',
  `type` int(10) DEFAULT NULL COMMENT '类型',
  `sort` varchar(10) DEFAULT NULL COMMENT '排序',
  `remark` varchar(100) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jsh_depothead
-- ----------------------------
DROP TABLE IF EXISTS `jsh_depothead`;
CREATE TABLE `jsh_depothead` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Type` varchar(50) DEFAULT NULL COMMENT '类型(出库/入库)',
  `SubType` varchar(50) DEFAULT NULL COMMENT '出入库分类',
  `ProjectId` bigint(20) DEFAULT NULL COMMENT '项目Id',
  `DefaultNumber` varchar(50) DEFAULT NULL COMMENT '初始票据号',
  `Number` varchar(50) DEFAULT NULL COMMENT '票据号',
  `OperPersonName` varchar(50) DEFAULT NULL COMMENT '操作员名字',
  `CreateTime` datetime DEFAULT NULL COMMENT '创建时间',
  `OperTime` datetime DEFAULT NULL COMMENT '出入库时间',
  `OrganId` bigint(20) DEFAULT NULL COMMENT '供应商Id',
  `HandsPersonId` bigint(20) DEFAULT NULL COMMENT '采购/领料-经手人Id',
  `AccountId` bigint(20) DEFAULT NULL COMMENT '账户Id',
  `ChangeAmount` double DEFAULT NULL COMMENT '变动金额(收款/付款)',
  `AllocationProjectId` bigint(20) DEFAULT NULL COMMENT '调拨时，对方项目Id',
  `TotalPrice` double DEFAULT NULL COMMENT '合计金额',
  `PayType` varchar(50) DEFAULT NULL,
  `Remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `Salesman` varchar(50) DEFAULT NULL COMMENT '业务员（可以多个）',
  `AccountIdList` varchar(50) DEFAULT NULL COMMENT '多账户ID列表',
  `AccountMoneyList` varchar(200) DEFAULT '' COMMENT '多账户金额列表',
  `Discount` double DEFAULT NULL COMMENT '优惠率',
  `DiscountMoney` double DEFAULT NULL COMMENT '优惠金额',
  `DiscountLastMoney` double DEFAULT NULL COMMENT '优惠后金额',
  `OtherMoney` double DEFAULT NULL COMMENT '销售或采购费用合计',
  `OtherMoneyList` varchar(200) DEFAULT NULL COMMENT '销售或采购费用涉及项目Id数组（包括快递、招待等）',
  `OtherMoneyItem` varchar(200) DEFAULT NULL COMMENT '销售或采购费用涉及项目（包括快递、招待等）',
  `AccountDay` int(10) DEFAULT NULL COMMENT '结算天数',
  `Status` bit(1) DEFAULT NULL COMMENT '单据状态(未审核、已审核)',
  PRIMARY KEY (`Id`),
  KEY `FK2A80F214CA633ABA` (`AllocationProjectId`),
  KEY `FK2A80F214C4170B37` (`HandsPersonId`),
  KEY `FK2A80F214B610FC06` (`OrganId`),
  KEY `FK2A80F2142888F9A` (`ProjectId`),
  KEY `FK2A80F214AAE50527` (`AccountId`),
  CONSTRAINT `FK2A80F214AAE50527` FOREIGN KEY (`AccountId`) REFERENCES `jsh_account` (`Id`),
  CONSTRAINT `jsh_depothead_ibfk_1` FOREIGN KEY (`ProjectId`) REFERENCES `jsh_depot` (`id`),
  CONSTRAINT `jsh_depothead_ibfk_3` FOREIGN KEY (`OrganId`) REFERENCES `jsh_supplier` (`id`),
  CONSTRAINT `jsh_depothead_ibfk_4` FOREIGN KEY (`HandsPersonId`) REFERENCES `jsh_person` (`Id`),
  CONSTRAINT `jsh_depothead_ibfk_5` FOREIGN KEY (`AllocationProjectId`) REFERENCES `jsh_depot` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jsh_depotitem
-- ----------------------------
DROP TABLE IF EXISTS `jsh_depotitem`;
CREATE TABLE `jsh_depotitem` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `HeaderId` bigint(20) NOT NULL COMMENT '表头Id',
  `MaterialId` bigint(20) NOT NULL COMMENT '材料Id',
  `MUnit` varchar(20) DEFAULT NULL COMMENT '商品计量单位',
  `OperNumber` double DEFAULT NULL COMMENT '数量',
  `BasicNumber` double DEFAULT NULL COMMENT '基础数量，如kg、瓶',
  `UnitPrice` double DEFAULT NULL COMMENT '单价',
  `TaxUnitPrice` double DEFAULT NULL COMMENT '含税单价',
  `AllPrice` double DEFAULT NULL COMMENT '金额',
  `Remark` varchar(200) DEFAULT NULL COMMENT '描述',
  `Img` varchar(50) DEFAULT NULL COMMENT '图片',
  `Incidentals` double DEFAULT NULL COMMENT '运杂费',
  `DepotId` bigint(20) DEFAULT NULL COMMENT '仓库ID（库存是统计出来的）',
  `AnotherDepotId` bigint(20) DEFAULT NULL COMMENT '调拨时，对方仓库Id',
  `TaxRate` double DEFAULT NULL COMMENT '税率',
  `TaxMoney` double DEFAULT NULL COMMENT '税额',
  `TaxLastMoney` double DEFAULT NULL COMMENT '价税合计',
  `OtherField1` varchar(50) DEFAULT NULL COMMENT '自定义字段1-品名',
  `OtherField2` varchar(50) DEFAULT NULL COMMENT '自定义字段2-型号',
  `OtherField3` varchar(50) DEFAULT NULL COMMENT '自定义字段3-制造商',
  `OtherField4` varchar(50) DEFAULT NULL COMMENT '自定义字段4',
  `OtherField5` varchar(50) DEFAULT NULL COMMENT '自定义字段5',
  `MType` varchar(20) DEFAULT NULL COMMENT '商品类型',
  PRIMARY KEY (`Id`),
  KEY `FK2A819F475D61CCF7` (`MaterialId`),
  KEY `FK2A819F474BB6190E` (`HeaderId`),
  KEY `FK2A819F479485B3F5` (`DepotId`),
  KEY `FK2A819F47729F5392` (`AnotherDepotId`),
  CONSTRAINT `FK2A819F47729F5392` FOREIGN KEY (`AnotherDepotId`) REFERENCES `jsh_depot` (`id`),
  CONSTRAINT `FK2A819F479485B3F5` FOREIGN KEY (`DepotId`) REFERENCES `jsh_depot` (`id`),
  CONSTRAINT `jsh_depotitem_ibfk_1` FOREIGN KEY (`HeaderId`) REFERENCES `jsh_depothead` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `jsh_depotitem_ibfk_2` FOREIGN KEY (`MaterialId`) REFERENCES `jsh_material` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jsh_functions
-- ----------------------------
DROP TABLE IF EXISTS `jsh_functions`;
CREATE TABLE `jsh_functions` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Number` varchar(50) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `PNumber` varchar(50) DEFAULT NULL,
  `URL` varchar(100) DEFAULT NULL,
  `State` bit(1) DEFAULT NULL,
  `Sort` varchar(50) DEFAULT NULL,
  `Enabled` bit(1) DEFAULT NULL,
  `Type` varchar(50) DEFAULT NULL,
  `PushBtn` varchar(50) DEFAULT NULL COMMENT '功能按钮',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=238 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jsh_functions
-- ----------------------------
INSERT INTO `jsh_functions` VALUES ('1', '00', '系统管理', '0', '', '', '0010', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('2', '01', '基础数据', '0', '', '', '0020', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('11', '0001', '系统管理', '00', '', '\0', '0110', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('12', '000101', '应用管理', '0001', '../manage/app.jsp', '\0', '0132', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('13', '000102', '角色管理', '0001', '../manage/role.jsp', '\0', '0130', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('14', '000103', '用户管理', '0001', '../manage/user.jsp', '\0', '0140', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('15', '000104', '日志管理', '0001', '../manage/log.jsp', '\0', '0160', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('16', '000105', '功能管理', '0001', '../manage/functions.jsp', '\0', '0135', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('21', '0101', '商品管理', '01', '', '\0', '0220', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('22', '010101', '商品类别', '0101', '../materials/materialcategory.jsp', '\0', '0230', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('23', '010102', '商品信息', '0101', '../materials/material.jsp', '\0', '0240', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('24', '0102', '基本资料', '01', '', '\0', '0250', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('25', '01020101', '供应商信息', '0102', '../manage/vendor.jsp', '\0', '0260', '', '电脑版', '1,2');
INSERT INTO `jsh_functions` VALUES ('26', '010202', '仓库信息', '0102', '../manage/depot.jsp', '\0', '0270', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('31', '010206', '经手人管理', '0102', '../materials/person.jsp', '\0', '0284', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('32', '0502', '入库管理', '05', '', '\0', '0330', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('33', '050201', '采购入库', '0502', '../materials/purchase_in_list.jsp', '\0', '0340', '', '电脑版', '3,4,5');
INSERT INTO `jsh_functions` VALUES ('38', '0603', '出库管理', '06', '', '\0', '0390', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('40', '060306', '调拨出库', '0603', '../materials/allocation_out_list.jsp', '\0', '0420', '', '电脑版', '3,4,5');
INSERT INTO `jsh_functions` VALUES ('41', '060303', '销售出库', '0603', '../materials/sale_out_list.jsp', '\0', '0410', '', '电脑版', '3,4,5');
INSERT INTO `jsh_functions` VALUES ('44', '0704', '财务管理', '07', '', '\0', '0450', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('59', '030101', '库存状况', '0301', '../reports/in_out_stock_report.jsp', '\0', '0600', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('194', '010204', '收支项目', '0102', '../manage/inOutItem.jsp', '\0', '0282', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('195', '010205', '结算账户', '0102', '../manage/account.jsp', '\0', '0283', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('196', '03', '报表查询', '0', '', '', '0025', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('197', '070402', '收入单', '0704', '../financial/item_in.jsp', '\0', '0465', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('198', '0301', '报表查询', '03', '', '\0', '0570', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('199', '060304', '采购退货', '0603', '../materials/purchase_back_list.jsp', '\0', '0415', '', '电脑版', '3,4,5');
INSERT INTO `jsh_functions` VALUES ('200', '050203', '销售退货', '0502', '../materials/sale_back_list.jsp', '\0', '0350', '', '电脑版', '3,4,5');
INSERT INTO `jsh_functions` VALUES ('201', '050204', '其它入库', '0502', '../materials/other_in_list.jsp', '\0', '0360', '', '电脑版', '3,4,5');
INSERT INTO `jsh_functions` VALUES ('202', '060305', '其它出库', '0603', '../materials/other_out_list.jsp', '\0', '0418', '', '电脑版', '3,4,5');
INSERT INTO `jsh_functions` VALUES ('203', '070403', '支出单', '0704', '../financial/item_out.jsp', '\0', '0470', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('204', '070404', '收款单', '0704', '../financial/money_in.jsp', '\0', '0475', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('205', '070405', '付款单', '0704', '../financial/money_out.jsp', '\0', '0480', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('206', '070406', '转账单', '0704', '../financial/giro.jsp', '\0', '0490', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('207', '030102', '结算账户', '0301', '../reports/account_report.jsp', '\0', '0610', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('208', '030103', '进货统计', '0301', '../reports/buy_in_report.jsp', '\0', '0620', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('209', '030104', '销售统计', '0301', '../reports/sale_out_report.jsp', '\0', '0630', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('210', '040102', '零售出库', '0401', '../materials/retail_out_list.jsp', '\0', '0405', '', '电脑版', '3,4,5');
INSERT INTO `jsh_functions` VALUES ('211', '040104', '零售退货', '0401', '../materials/retail_back_list.jsp', '\0', '0407', '', '电脑版', '3,4,5');
INSERT INTO `jsh_functions` VALUES ('212', '070407', '收预付款', '0704', '../financial/advance_in.jsp', '\0', '0495', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('213', '010207', '礼品卡管理', '0102', '../manage/depotGift.jsp', '\0', '0290', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('214', '040106', '礼品充值', '0401', '../materials/gift_recharge_list.jsp', '\0', '0408', '', '电脑版', '3,4,5');
INSERT INTO `jsh_functions` VALUES ('215', '040108', '礼品销售', '0401', '../materials/gift_out_list.jsp', '\0', '0409', '', '电脑版', '3,4,5');
INSERT INTO `jsh_functions` VALUES ('216', '030105', '礼品卡统计', '0301', '../reports/gift_manage_report.jsp', '\0', '0635', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('217', '01020102', '客户信息', '0102', '../manage/customer.jsp', '\0', '0262', '', '电脑版', '1,2');
INSERT INTO `jsh_functions` VALUES ('218', '01020103', '会员信息', '0102', '../manage/member.jsp', '\0', '0263', '', '电脑版', '1,2');
INSERT INTO `jsh_functions` VALUES ('219', '000107', '资产管理', '0001', '../asset/asset.jsp', '\0', '0170', '\0', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('220', '010103', '计量单位', '0101', '../manage/unit.jsp', '\0', '0245', '', '电脑版', null);
INSERT INTO `jsh_functions` VALUES ('221', '04', '零售管理', '0', '', '', '0028', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('222', '05', '入库管理', '0', '', '', '0030', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('223', '06', '出库管理', '0', '', '', '0035', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('224', '07', '财务管理', '0', '', '', '0040', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('225', '0401', '零售管理', '04', '', '\0', '0401', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('226', '030106', '入库明细', '0301', '../reports/in_detail.jsp', '\0', '0640', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('227', '030107', '出库明细', '0301', '../reports/out_detail.jsp', '\0', '0645', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('228', '030108', '入库汇总', '0301', '../reports/in_material_count.jsp', '\0', '0650', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('229', '030109', '出库汇总', '0301', '../reports/out_material_count.jsp', '\0', '0655', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('230', '02', '组装拆卸', '0', '', '', '0022', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('231', '0201', '组装拆卸', '02', '', '\0', '0310', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('232', '020101', '组装单', '0201', '../materials/assemble_list.jsp', '\0', '0315', '', '电脑版', '3,4,5');
INSERT INTO `jsh_functions` VALUES ('233', '020102', '拆卸单', '0201', '../materials/disassemble_list.jsp', '\0', '0320', '', '电脑版', '3,4,5');
INSERT INTO `jsh_functions` VALUES ('234', '000105', '系统配置', '0001', '../manage/systemConfig.jsp', '\0', '0165', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('235', '030110', '客户对账', '0301', '../reports/customer_account.jsp', '\0', '0660', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('236', '000106', '商品属性', '0001', '../materials/materialProperty.jsp', '\0', '0168', '', '电脑版', '');
INSERT INTO `jsh_functions` VALUES ('237', '030111', '供应商对账', '0301', '../reports/vendor_account.jsp', '\0', '0665', '', '电脑版', '');

-- ----------------------------
-- Table structure for jsh_inoutitem
-- ----------------------------
DROP TABLE IF EXISTS `jsh_inoutitem`;
CREATE TABLE `jsh_inoutitem` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Type` varchar(20) DEFAULT NULL COMMENT '类型',
  `Remark` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jsh_log
-- ----------------------------
DROP TABLE IF EXISTS `jsh_log`;
CREATE TABLE `jsh_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) NOT NULL COMMENT '操作用户ID',
  `operation` varchar(500) DEFAULT NULL COMMENT '操作模块名称',
  `clientIP` varchar(50) DEFAULT NULL COMMENT '客户端IP',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `status` tinyint(4) DEFAULT NULL COMMENT '操作状态 0==成功，1==失败',
  `contentdetails` varchar(1000) DEFAULT NULL COMMENT '操作详情',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `FKF2696AA13E226853` (`userID`),
  CONSTRAINT `FKF2696AA13E226853` FOREIGN KEY (`userID`) REFERENCES `jsh_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6115 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jsh_material
-- ----------------------------
DROP TABLE IF EXISTS `jsh_material`;
CREATE TABLE `jsh_material` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CategoryId` bigint(20) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Mfrs` varchar(50) DEFAULT NULL COMMENT '制造商',
  `Packing` double DEFAULT NULL COMMENT '包装（KG/包）',
  `SafetyStock` double DEFAULT NULL COMMENT '安全存量（KG）',
  `Model` varchar(50) DEFAULT NULL COMMENT '型号',
  `Standard` varchar(50) DEFAULT NULL COMMENT '规格',
  `Color` varchar(50) DEFAULT NULL COMMENT '颜色',
  `Unit` varchar(50) DEFAULT NULL COMMENT '单位-单个',
  `Remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `RetailPrice` double DEFAULT NULL COMMENT '零售价',
  `LowPrice` double DEFAULT NULL COMMENT '最低售价',
  `PresetPriceOne` double DEFAULT NULL COMMENT '预设售价一',
  `PresetPriceTwo` double DEFAULT NULL COMMENT '预设售价二',
  `UnitId` bigint(20) DEFAULT NULL COMMENT '计量单位Id',
  `FirstOutUnit` varchar(50) DEFAULT NULL COMMENT '首选出库单位',
  `FirstInUnit` varchar(50) DEFAULT NULL COMMENT '首选入库单位',
  `PriceStrategy` varchar(500) DEFAULT NULL COMMENT '价格策略',
  `Enabled` bit(1) DEFAULT NULL COMMENT '启用 0-禁用  1-启用',
  `OtherField1` varchar(50) DEFAULT NULL COMMENT '自定义1',
  `OtherField2` varchar(50) DEFAULT NULL COMMENT '自定义2',
  `OtherField3` varchar(50) DEFAULT NULL COMMENT '自定义3',
  PRIMARY KEY (`Id`),
  KEY `FK675951272AB6672C` (`CategoryId`),
  KEY `UnitId` (`UnitId`),
  CONSTRAINT `FK675951272AB6672C` FOREIGN KEY (`CategoryId`) REFERENCES `jsh_materialcategory` (`Id`),
  CONSTRAINT `jsh_material_ibfk_1` FOREIGN KEY (`UnitId`) REFERENCES `jsh_unit` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=567 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jsh_materialcategory
-- ----------------------------
DROP TABLE IF EXISTS `jsh_materialcategory`;
CREATE TABLE `jsh_materialcategory` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `CategoryLevel` smallint(6) DEFAULT NULL COMMENT '等级',
  `ParentId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK3EE7F725237A77D8` (`ParentId`),
  CONSTRAINT `FK3EE7F725237A77D8` FOREIGN KEY (`ParentId`) REFERENCES `jsh_materialcategory` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jsh_materialproperty
-- ----------------------------
DROP TABLE IF EXISTS `jsh_materialproperty`;
CREATE TABLE `jsh_materialproperty` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nativeName` varchar(50) DEFAULT NULL COMMENT '原始名称',
  `enabled` bit(1) DEFAULT NULL COMMENT '是否启用',
  `sort` varchar(10) DEFAULT NULL COMMENT '排序',
  `anotherName` varchar(50) DEFAULT NULL COMMENT '别名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jsh_materialproperty
-- ----------------------------
INSERT INTO `jsh_materialproperty` VALUES ('1', '规格', '', '02', '规格');
INSERT INTO `jsh_materialproperty` VALUES ('2', '颜色', '', '01', '颜色');
INSERT INTO `jsh_materialproperty` VALUES ('3', '制造商', '', '03', '制造商');
INSERT INTO `jsh_materialproperty` VALUES ('4', '自定义1', '\0', '04', '自定义1');
INSERT INTO `jsh_materialproperty` VALUES ('5', '自定义2', '\0', '05', '自定义2');
INSERT INTO `jsh_materialproperty` VALUES ('6', '自定义3', '\0', '06', '自定义3');

-- ----------------------------
-- Table structure for jsh_person
-- ----------------------------
DROP TABLE IF EXISTS `jsh_person`;
CREATE TABLE `jsh_person` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Type` varchar(20) DEFAULT NULL COMMENT '类型',
  `Name` varchar(50) DEFAULT NULL COMMENT '姓名',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jsh_role
-- ----------------------------
DROP TABLE IF EXISTS `jsh_role`;
CREATE TABLE `jsh_role` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL COMMENT '类型',
  `value` varchar(200) DEFAULT NULL COMMENT '值',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jsh_role
-- ----------------------------
INSERT INTO `jsh_role` VALUES ('4', '管理员', null, null, null);
INSERT INTO `jsh_role` VALUES ('8', '超级管理员', null, null, null);
INSERT INTO `jsh_role` VALUES ('9', '业务员', null, null, null);
INSERT INTO `jsh_role` VALUES ('10', '财务', null, null, null);

-- ----------------------------
-- Table structure for jsh_supplier
-- ----------------------------
DROP TABLE IF EXISTS `jsh_supplier`;
CREATE TABLE `jsh_supplier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `supplier` varchar(255) NOT NULL COMMENT '供应商名称',
  `contacts` varchar(100) DEFAULT NULL COMMENT '联系人',
  `phonenum` varchar(30) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '电子邮箱',
  `description` varchar(500) DEFAULT NULL,
  `isystem` tinyint(4) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL COMMENT '类型',
  `enabled` bit(1) DEFAULT NULL COMMENT '启用',
  `AdvanceIn` double DEFAULT '0',
  `BeginNeedGet` double DEFAULT NULL COMMENT '期初应收',
  `BeginNeedPay` double DEFAULT NULL COMMENT '期初应付',
  `AllNeedGet` double DEFAULT NULL COMMENT '累计应收',
  `AllNeedPay` double DEFAULT NULL COMMENT '累计应付',
  `fax` varchar(30) DEFAULT NULL COMMENT '传真',
  `telephone` varchar(30) DEFAULT NULL COMMENT '手机',
  `address` varchar(50) DEFAULT NULL COMMENT '地址',
  `taxNum` varchar(50) DEFAULT NULL COMMENT '纳税人识别号',
  `bankName` varchar(50) DEFAULT NULL COMMENT '开户行',
  `accountNumber` varchar(50) DEFAULT NULL COMMENT '账号',
  `taxRate` double DEFAULT NULL COMMENT '税率',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jsh_systemconfig
-- ----------------------------
DROP TABLE IF EXISTS `jsh_systemconfig`;
CREATE TABLE `jsh_systemconfig` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `type` varchar(50) DEFAULT NULL COMMENT '类型',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `value` varchar(200) DEFAULT NULL COMMENT '值',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统参数';

-- ----------------------------
-- Records of jsh_systemconfig
-- ----------------------------
INSERT INTO `jsh_systemconfig` VALUES ('1', 'basic', 'company_name', '兰澜搭配', '公司名称');
INSERT INTO `jsh_systemconfig` VALUES ('2', 'basic', 'company_contacts', '王兰', '公司联系人');
INSERT INTO `jsh_systemconfig` VALUES ('3', 'basic', 'company_address', '皮革城', '公司地址');
INSERT INTO `jsh_systemconfig` VALUES ('4', 'basic', 'company_tel', '0513-10101010', '公司电话');
INSERT INTO `jsh_systemconfig` VALUES ('5', 'basic', 'company_fax', '0513-18181818', '公司传真');
INSERT INTO `jsh_systemconfig` VALUES ('6', 'basic', 'company_post_code', '226300', '公司邮编');

-- ----------------------------
-- Table structure for jsh_unit
-- ----------------------------
DROP TABLE IF EXISTS `jsh_unit`;
CREATE TABLE `jsh_unit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `UName` varchar(50) DEFAULT NULL COMMENT '名称，支持多单位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jsh_user
-- ----------------------------
DROP TABLE IF EXISTS `jsh_user`;
CREATE TABLE `jsh_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL COMMENT '用户姓名--例如张三',
  `loginame` varchar(255) DEFAULT NULL COMMENT '登录用户名--可能为空',
  `password` varchar(50) DEFAULT NULL COMMENT '登陆密码',
  `position` varchar(200) DEFAULT NULL COMMENT '职位',
  `department` varchar(255) DEFAULT NULL COMMENT '所属部门',
  `email` varchar(100) DEFAULT NULL COMMENT '电子邮箱',
  `phonenum` varchar(100) DEFAULT NULL COMMENT '手机号码',
  `ismanager` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否为管理者 0==管理者 1==员工',
  `isystem` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否系统自带数据 ',
  `status` tinyint(4) DEFAULT NULL COMMENT '用户状态',
  `description` varchar(500) DEFAULT NULL COMMENT '用户描述信息',
  `remark` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jsh_user
-- ----------------------------
INSERT INTO `jsh_user` VALUES ('66', 'Asker', 'Asker', 'e10adc3949ba59abbe56e057f20f883e', 'Java研发工程师', '开发部', 'yy8309417@sina.com', '10086', '1', '1', null, 'admin', null);
INSERT INTO `jsh_user` VALUES ('69', '王兰', 'wanglan', 'e10adc3949ba59abbe56e057f20f883e', '店长', '', '', '', '1', '1', null, '', null);
INSERT INTO `jsh_user` VALUES ('70', '红敏', '0001', 'e10adc3949ba59abbe56e057f20f883e', '业务员', '', '', '', '1', '1', null, '', null);
INSERT INTO `jsh_user` VALUES ('71', '杨旭', 'yangxu', 'e10adc3949ba59abbe56e057f20f883e', '财务', '', '', '', '1', '1', null, '', null);

-- ----------------------------
-- Table structure for jsh_userbusiness
-- ----------------------------
DROP TABLE IF EXISTS `jsh_userbusiness`;
CREATE TABLE `jsh_userbusiness` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Type` varchar(50) DEFAULT NULL COMMENT '类别',
  `KeyId` varchar(50) DEFAULT NULL COMMENT '主ID',
  `Value` varchar(10000) DEFAULT NULL COMMENT '值',
  `BtnStr` varchar(2000) DEFAULT NULL COMMENT '按钮权限',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jsh_userbusiness
-- ----------------------------
INSERT INTO `jsh_userbusiness` VALUES ('1', 'RoleAPP', '4', '[23][24][25][8][26][22][7][3][6]', null);
INSERT INTO `jsh_userbusiness` VALUES ('2', 'RoleFunctions', '4', '[13][12][16][14][15][234][236][22][23][220][25][217][218][26][194][195][31][213][232][233][59][207][208][209][216][226][227][228][229][235][237][210][211][214][215][33][200][201][41][199][202][40][197][203][204][205][206][212]', '[{\"funId\":\"25\",\"btnStr\":\"1,2\"},{\"funId\":\"217\",\"btnStr\":\"1,2\"},{\"funId\":\"218\",\"btnStr\":\"1,2\"},{\"funId\":\"232\",\"btnStr\":\"3,4,5\"},{\"funId\":\"233\",\"btnStr\":\"3,4,5\"},{\"funId\":\"33\",\"btnStr\":\"3,4,5\"},{\"funId\":\"200\",\"btnStr\":\"3,4,5\"},{\"funId\":\"201\",\"btnStr\":\"3,4,5\"},{\"funId\":\"210\",\"btnStr\":\"3,4,5\"},{\"funId\":\"211\",\"btnStr\":\"3,4,5\"},{\"funId\":\"214\",\"btnStr\":\"3,4,5\"},{\"funId\":\"215\",\"btnStr\":\"3,4,5\"},{\"funId\":\"41\",\"btnStr\":\"3,4,5\"},{\"funId\":\"199\",\"btnStr\":\"3,4,5\"},{\"funId\":\"202\",\"btnStr\":\"3,4,5\"},{\"funId\":\"40\",\"btnStr\":\"3,4,5\"}]');
INSERT INTO `jsh_userbusiness` VALUES ('3', 'UserRole', '66', '[4]', null);
INSERT INTO `jsh_userbusiness` VALUES ('39', 'RoleAPP', '8', '[23][24][25][8][26][22][7][3][6]', null);
INSERT INTO `jsh_userbusiness` VALUES ('40', 'RoleFunctions', '8', '[13][12][16][14][15][234][236][22][23][220][25][217][218][26][194][195][31][213][232][233][59][207][208][209][216][226][227][228][229][235][237][210][211][214][215][33][200][201][41][199][202][40][197][203][204][205][206][212]', '[{\"funId\":\"25\",\"btnStr\":\"1,2\"},{\"funId\":\"217\",\"btnStr\":\"1,2\"},{\"funId\":\"218\",\"btnStr\":\"1,2\"},{\"funId\":\"232\",\"btnStr\":\"3,4,5\"},{\"funId\":\"233\",\"btnStr\":\"3,4,5\"},{\"funId\":\"33\",\"btnStr\":\"3,4,5\"},{\"funId\":\"200\",\"btnStr\":\"3,4,5\"},{\"funId\":\"201\",\"btnStr\":\"3,4,5\"},{\"funId\":\"210\",\"btnStr\":\"3,4,5\"},{\"funId\":\"211\",\"btnStr\":\"3,4,5\"},{\"funId\":\"214\",\"btnStr\":\"3,4,5\"},{\"funId\":\"215\",\"btnStr\":\"3,4,5\"},{\"funId\":\"41\",\"btnStr\":\"3,4,5\"},{\"funId\":\"199\",\"btnStr\":\"3,4,5\"},{\"funId\":\"202\",\"btnStr\":\"3,4,5\"},{\"funId\":\"40\",\"btnStr\":\"3,4,5\"}]');
INSERT INTO `jsh_userbusiness` VALUES ('41', 'UserRole', '69', '[8]', null);
INSERT INTO `jsh_userbusiness` VALUES ('42', 'RoleAPP', '9', '[23][6]', null);
INSERT INTO `jsh_userbusiness` VALUES ('43', 'RoleFunctions', '9', '[210][211][214][215]', null);
INSERT INTO `jsh_userbusiness` VALUES ('44', 'UserRole', '70', '[9]', null);
INSERT INTO `jsh_userbusiness` VALUES ('45', 'RoleAPP', '10', '[26][6]', null);
INSERT INTO `jsh_userbusiness` VALUES ('46', 'RoleFunctions', '10', '[197][203][204][205][206][212]', null);
INSERT INTO `jsh_userbusiness` VALUES ('47', 'UserRole', '71', '[10]', null);

-- ----------------------------
-- Table structure for jsh_visitaccount
-- ----------------------------
DROP TABLE IF EXISTS `jsh_visitaccount`;
CREATE TABLE `jsh_visitaccount` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ProjectId` bigint(20) NOT NULL,
  `LouHao` varchar(50) DEFAULT NULL,
  `HuHao` varchar(50) DEFAULT NULL,
  `HuiFang` varchar(50) DEFAULT NULL,
  `LuoShi` varchar(50) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Tel` varchar(50) DEFAULT NULL,
  `AddTime` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFF4AAE822888F9A` (`ProjectId`),
  CONSTRAINT `FKFF4AAE822888F9A` FOREIGN KEY (`ProjectId`) REFERENCES `jsh_depot` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
