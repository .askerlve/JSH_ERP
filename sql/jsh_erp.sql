/*
MySQL Backup
Source Server Version: 5.0.22
Source Database: jsh_erp
Date: 2017-12-10 23:12:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
--  Table structure for `databasechangelog`
-- ----------------------------
DROP TABLE IF EXISTS `databasechangelog`;
CREATE TABLE `databasechangelog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) default NULL,
  `DESCRIPTION` varchar(255) default NULL,
  `COMMENTS` varchar(255) default NULL,
  `TAG` varchar(255) default NULL,
  `LIQUIBASE` varchar(20) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `databasechangeloglock`
-- ----------------------------
DROP TABLE IF EXISTS `databasechangeloglock`;
CREATE TABLE `databasechangeloglock` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime default NULL,
  `LOCKEDBY` varchar(255) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_account`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_account`;
CREATE TABLE `jsh_account` (
  `Id` bigint(20) NOT NULL auto_increment,
  `Name` varchar(50) default NULL COMMENT '名称',
  `SerialNo` varchar(50) default NULL COMMENT '编号',
  `InitialAmount` double default NULL COMMENT '期初金额',
  `CurrentAmount` double default NULL COMMENT '当前余额',
  `Remark` varchar(100) default NULL COMMENT '备注',
  `IsDefault` bit(1) default NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_accounthead`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_accounthead`;
CREATE TABLE `jsh_accounthead` (
  `Id` bigint(20) NOT NULL auto_increment,
  `Type` varchar(50) default NULL COMMENT '类型(支出/收入/收款/付款/转账)',
  `OrganId` bigint(20) default NULL COMMENT '单位Id(收款/付款单位)',
  `HandsPersonId` bigint(20) default NULL COMMENT '经手人Id',
  `ChangeAmount` double default NULL COMMENT '变动金额(优惠/收款/付款/实付)',
  `TotalPrice` double default NULL COMMENT '合计金额',
  `AccountId` bigint(20) default NULL COMMENT '账户(收款/付款)',
  `BillNo` varchar(50) default NULL COMMENT '单据编号',
  `BillTime` datetime default NULL COMMENT '单据日期',
  `Remark` varchar(100) default NULL COMMENT '备注',
  PRIMARY KEY  (`Id`),
  KEY `FK9F4C0D8DB610FC06` (`OrganId`),
  KEY `FK9F4C0D8DAAE50527` (`AccountId`),
  KEY `FK9F4C0D8DC4170B37` (`HandsPersonId`),
  CONSTRAINT `FK9F4C0D8DAAE50527` FOREIGN KEY (`AccountId`) REFERENCES `jsh_account` (`Id`),
  CONSTRAINT `FK9F4C0D8DB610FC06` FOREIGN KEY (`OrganId`) REFERENCES `jsh_supplier` (`id`),
  CONSTRAINT `FK9F4C0D8DC4170B37` FOREIGN KEY (`HandsPersonId`) REFERENCES `jsh_person` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_accountitem`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_accountitem`;
CREATE TABLE `jsh_accountitem` (
  `Id` bigint(20) NOT NULL auto_increment,
  `HeaderId` bigint(20) NOT NULL COMMENT '表头Id',
  `AccountId` bigint(20) default NULL COMMENT '账户Id',
  `InOutItemId` bigint(20) default NULL COMMENT '收支项目Id',
  `EachAmount` double default NULL COMMENT '单项金额',
  `Remark` varchar(100) default NULL COMMENT '单据备注',
  PRIMARY KEY  (`Id`),
  KEY `FK9F4CBAC0AAE50527` (`AccountId`),
  KEY `FK9F4CBAC0C5FE6007` (`HeaderId`),
  KEY `FK9F4CBAC0D203EDC5` (`InOutItemId`),
  CONSTRAINT `FK9F4CBAC0AAE50527` FOREIGN KEY (`AccountId`) REFERENCES `jsh_account` (`Id`),
  CONSTRAINT `FK9F4CBAC0C5FE6007` FOREIGN KEY (`HeaderId`) REFERENCES `jsh_accounthead` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK9F4CBAC0D203EDC5` FOREIGN KEY (`InOutItemId`) REFERENCES `jsh_inoutitem` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_app`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_app`;
CREATE TABLE `jsh_app` (
  `Id` bigint(20) NOT NULL auto_increment,
  `Number` varchar(50) default NULL,
  `Name` varchar(50) default NULL,
  `Type` varchar(50) default NULL,
  `Icon` varchar(50) default NULL,
  `URL` varchar(50) default NULL,
  `Width` varchar(50) default NULL,
  `Height` varchar(50) default NULL,
  `ReSize` bit(1) default NULL,
  `OpenMax` bit(1) default NULL,
  `Flash` bit(1) default NULL,
  `ZL` varchar(50) default NULL,
  `Sort` varchar(50) default NULL,
  `Remark` varchar(200) default NULL,
  `Enabled` bit(1) default NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_asset`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_asset`;
CREATE TABLE `jsh_asset` (
  `id` bigint(20) NOT NULL auto_increment,
  `assetnameID` bigint(20) NOT NULL,
  `location` varchar(255) default NULL COMMENT '位置',
  `labels` varchar(255) default NULL COMMENT '标签：以空格为分隔符',
  `status` smallint(6) default NULL COMMENT '资产的状态：0==在库，1==在用，2==消费',
  `userID` bigint(20) default NULL,
  `price` double default NULL COMMENT '购买价格',
  `purchasedate` datetime default NULL COMMENT '购买日期',
  `periodofvalidity` datetime default NULL COMMENT '有效日期',
  `warrantydate` datetime default NULL COMMENT '保修日期',
  `assetnum` varchar(255) default NULL COMMENT '资产编号',
  `serialnum` varchar(255) default NULL COMMENT '资产序列号',
  `supplier` bigint(20) NOT NULL,
  `description` longtext COMMENT '描述信息',
  `addMonth` longtext COMMENT '资产添加时间，统计报表使用',
  `createtime` datetime default NULL,
  `creator` bigint(20) default NULL,
  `updatetime` datetime default NULL,
  `updator` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK353690ED9B6CB285` (`assetnameID`),
  KEY `FK353690EDAD45B659` (`creator`),
  KEY `FK353690ED27D23FE4` (`supplier`),
  KEY `FK353690ED61FE182C` (`updator`),
  KEY `FK353690ED3E226853` (`userID`),
  CONSTRAINT `FK353690ED27D23FE4` FOREIGN KEY (`supplier`) REFERENCES `jsh_supplier` (`id`),
  CONSTRAINT `FK353690ED3E226853` FOREIGN KEY (`userID`) REFERENCES `jsh_user` (`id`),
  CONSTRAINT `FK353690ED61FE182C` FOREIGN KEY (`updator`) REFERENCES `jsh_user` (`id`),
  CONSTRAINT `FK353690ED9B6CB285` FOREIGN KEY (`assetnameID`) REFERENCES `jsh_assetname` (`id`),
  CONSTRAINT `FK353690EDAD45B659` FOREIGN KEY (`creator`) REFERENCES `jsh_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_assetcategory`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_assetcategory`;
CREATE TABLE `jsh_assetcategory` (
  `id` bigint(20) NOT NULL auto_increment,
  `assetname` varchar(255) NOT NULL COMMENT '资产类型名称',
  `isystem` tinyint(4) NOT NULL COMMENT '是否系统自带 0==系统 1==非系统',
  `description` varchar(500) default NULL COMMENT '描述信息',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_assetname`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_assetname`;
CREATE TABLE `jsh_assetname` (
  `id` bigint(20) NOT NULL auto_increment,
  `assetname` varchar(255) NOT NULL COMMENT '资产名称',
  `assetcategoryID` bigint(20) NOT NULL,
  `isystem` smallint(6) NOT NULL COMMENT '是否系统自带 0==系统 1==非系统',
  `description` longtext COMMENT '描述信息',
  `isconsumables` smallint(6) default NULL COMMENT '是否为耗材 0==否 1==是 耗材状态只能是消费',
  PRIMARY KEY  (`id`),
  KEY `FKA4ADCCF866BC8AD3` (`assetcategoryID`),
  CONSTRAINT `FKA4ADCCF866BC8AD3` FOREIGN KEY (`assetcategoryID`) REFERENCES `jsh_assetcategory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_depot`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_depot`;
CREATE TABLE `jsh_depot` (
  `id` bigint(20) NOT NULL auto_increment COMMENT '主键',
  `name` varchar(20) default NULL COMMENT '仓库名称',
  `address` varchar(50) default NULL COMMENT '仓库地址',
  `warehousing` double default NULL COMMENT '仓储费',
  `truckage` double default NULL COMMENT '搬运费',
  `type` int(10) default NULL COMMENT '类型',
  `sort` varchar(10) default NULL COMMENT '排序',
  `remark` varchar(100) default NULL COMMENT '描述',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_depothead`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_depothead`;
CREATE TABLE `jsh_depothead` (
  `Id` bigint(20) NOT NULL auto_increment,
  `Type` varchar(50) default NULL COMMENT '类型(出库/入库)',
  `SubType` varchar(50) default NULL COMMENT '出入库分类',
  `ProjectId` bigint(20) default NULL COMMENT '项目Id',
  `DefaultNumber` varchar(50) default NULL COMMENT '初始票据号',
  `Number` varchar(50) default NULL COMMENT '票据号',
  `OperPersonName` varchar(50) default NULL COMMENT '操作员名字',
  `CreateTime` datetime default NULL COMMENT '创建时间',
  `OperTime` datetime default NULL COMMENT '出入库时间',
  `OrganId` bigint(20) default NULL COMMENT '供应商Id',
  `HandsPersonId` bigint(20) default NULL COMMENT '采购/领料-经手人Id',
  `AccountId` bigint(20) default NULL COMMENT '账户Id',
  `ChangeAmount` double default NULL COMMENT '变动金额(收款/付款)',
  `AllocationProjectId` bigint(20) default NULL COMMENT '调拨时，对方项目Id',
  `TotalPrice` double default NULL COMMENT '合计金额',
  `PayType` varchar(50) default NULL,
  `Remark` varchar(1000) default NULL COMMENT '备注',
  `Salesman` varchar(50) default NULL COMMENT '业务员（可以多个）',
  `AccountIdList` varchar(50) default NULL COMMENT '多账户ID列表',
  `AccountMoneyList` varchar(200) default '' COMMENT '多账户金额列表',
  `Discount` double default NULL COMMENT '优惠率',
  `DiscountMoney` double default NULL COMMENT '优惠金额',
  `DiscountLastMoney` double default NULL COMMENT '优惠后金额',
  `OtherMoney` double default NULL COMMENT '销售或采购费用合计',
  `OtherMoneyList` varchar(200) default NULL COMMENT '销售或采购费用涉及项目Id数组（包括快递、招待等）',
  `OtherMoneyItem` varchar(200) default NULL COMMENT '销售或采购费用涉及项目（包括快递、招待等）',
  `AccountDay` int(10) default NULL COMMENT '结算天数',
  `Status` bit(1) default NULL COMMENT '单据状态(未审核、已审核)',
  PRIMARY KEY  (`Id`),
  KEY `FK2A80F214CA633ABA` (`AllocationProjectId`),
  KEY `FK2A80F214C4170B37` (`HandsPersonId`),
  KEY `FK2A80F214B610FC06` (`OrganId`),
  KEY `FK2A80F2142888F9A` (`ProjectId`),
  KEY `FK2A80F214AAE50527` (`AccountId`),
  CONSTRAINT `FK2A80F214AAE50527` FOREIGN KEY (`AccountId`) REFERENCES `jsh_account` (`Id`),
  CONSTRAINT `jsh_depothead_ibfk_1` FOREIGN KEY (`ProjectId`) REFERENCES `jsh_depot` (`id`),
  CONSTRAINT `jsh_depothead_ibfk_3` FOREIGN KEY (`OrganId`) REFERENCES `jsh_supplier` (`id`),
  CONSTRAINT `jsh_depothead_ibfk_4` FOREIGN KEY (`HandsPersonId`) REFERENCES `jsh_person` (`Id`),
  CONSTRAINT `jsh_depothead_ibfk_5` FOREIGN KEY (`AllocationProjectId`) REFERENCES `jsh_depot` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_depotitem`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_depotitem`;
CREATE TABLE `jsh_depotitem` (
  `Id` bigint(20) NOT NULL auto_increment,
  `HeaderId` bigint(20) NOT NULL COMMENT '表头Id',
  `MaterialId` bigint(20) NOT NULL COMMENT '材料Id',
  `MUnit` varchar(20) default NULL COMMENT '商品计量单位',
  `OperNumber` double default NULL COMMENT '数量',
  `BasicNumber` double default NULL COMMENT '基础数量，如kg、瓶',
  `UnitPrice` double default NULL COMMENT '单价',
  `TaxUnitPrice` double default NULL COMMENT '含税单价',
  `AllPrice` double default NULL COMMENT '金额',
  `Remark` varchar(200) default NULL COMMENT '描述',
  `Img` varchar(50) default NULL COMMENT '图片',
  `Incidentals` double default NULL COMMENT '运杂费',
  `DepotId` bigint(20) default NULL COMMENT '仓库ID（库存是统计出来的）',
  `AnotherDepotId` bigint(20) default NULL COMMENT '调拨时，对方仓库Id',
  `TaxRate` double default NULL COMMENT '税率',
  `TaxMoney` double default NULL COMMENT '税额',
  `TaxLastMoney` double default NULL COMMENT '价税合计',
  `OtherField1` varchar(50) default NULL COMMENT '自定义字段1-品名',
  `OtherField2` varchar(50) default NULL COMMENT '自定义字段2-型号',
  `OtherField3` varchar(50) default NULL COMMENT '自定义字段3-制造商',
  `OtherField4` varchar(50) default NULL COMMENT '自定义字段4',
  `OtherField5` varchar(50) default NULL COMMENT '自定义字段5',
  `MType` varchar(20) default NULL COMMENT '商品类型',
  PRIMARY KEY  (`Id`),
  KEY `FK2A819F475D61CCF7` (`MaterialId`),
  KEY `FK2A819F474BB6190E` (`HeaderId`),
  KEY `FK2A819F479485B3F5` (`DepotId`),
  KEY `FK2A819F47729F5392` (`AnotherDepotId`),
  CONSTRAINT `FK2A819F47729F5392` FOREIGN KEY (`AnotherDepotId`) REFERENCES `jsh_depot` (`id`),
  CONSTRAINT `FK2A819F479485B3F5` FOREIGN KEY (`DepotId`) REFERENCES `jsh_depot` (`id`),
  CONSTRAINT `jsh_depotitem_ibfk_1` FOREIGN KEY (`HeaderId`) REFERENCES `jsh_depothead` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `jsh_depotitem_ibfk_2` FOREIGN KEY (`MaterialId`) REFERENCES `jsh_material` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_functions`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_functions`;
CREATE TABLE `jsh_functions` (
  `Id` bigint(20) NOT NULL auto_increment,
  `Number` varchar(50) default NULL,
  `Name` varchar(50) default NULL,
  `PNumber` varchar(50) default NULL,
  `URL` varchar(100) default NULL,
  `State` bit(1) default NULL,
  `Sort` varchar(50) default NULL,
  `Enabled` bit(1) default NULL,
  `Type` varchar(50) default NULL,
  `PushBtn` varchar(50) default NULL COMMENT '功能按钮',
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_inoutitem`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_inoutitem`;
CREATE TABLE `jsh_inoutitem` (
  `Id` bigint(20) NOT NULL auto_increment,
  `Name` varchar(50) default NULL COMMENT '名称',
  `Type` varchar(20) default NULL COMMENT '类型',
  `Remark` varchar(100) default NULL COMMENT '备注',
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_log`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_log`;
CREATE TABLE `jsh_log` (
  `id` bigint(20) NOT NULL auto_increment,
  `userID` bigint(20) NOT NULL COMMENT '操作用户ID',
  `operation` varchar(500) default NULL COMMENT '操作模块名称',
  `clientIP` varchar(50) default NULL COMMENT '客户端IP',
  `createtime` datetime default NULL COMMENT '创建时间',
  `status` tinyint(4) default NULL COMMENT '操作状态 0==成功，1==失败',
  `contentdetails` varchar(1000) default NULL COMMENT '操作详情',
  `remark` varchar(500) default NULL COMMENT '备注信息',
  PRIMARY KEY  (`id`),
  KEY `FKF2696AA13E226853` (`userID`),
  CONSTRAINT `FKF2696AA13E226853` FOREIGN KEY (`userID`) REFERENCES `jsh_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_material`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_material`;
CREATE TABLE `jsh_material` (
  `Id` bigint(20) NOT NULL auto_increment,
  `CategoryId` bigint(20) default NULL,
  `Name` varchar(50) default NULL COMMENT '名称',
  `Mfrs` varchar(50) default NULL COMMENT '制造商',
  `Packing` double default NULL COMMENT '包装（KG/包）',
  `SafetyStock` double default NULL COMMENT '安全存量（KG）',
  `Model` varchar(50) default NULL COMMENT '型号',
  `Standard` varchar(50) default NULL COMMENT '规格',
  `Color` varchar(50) default NULL COMMENT '颜色',
  `Unit` varchar(50) default NULL COMMENT '单位-单个',
  `Remark` varchar(100) default NULL COMMENT '备注',
  `RetailPrice` double default NULL COMMENT '零售价',
  `LowPrice` double default NULL COMMENT '最低售价',
  `PresetPriceOne` double default NULL COMMENT '预设售价一',
  `PresetPriceTwo` double default NULL COMMENT '预设售价二',
  `UnitId` bigint(20) default NULL COMMENT '计量单位Id',
  `FirstOutUnit` varchar(50) default NULL COMMENT '首选出库单位',
  `FirstInUnit` varchar(50) default NULL COMMENT '首选入库单位',
  `PriceStrategy` varchar(500) default NULL COMMENT '价格策略',
  `Enabled` bit(1) default NULL COMMENT '启用 0-禁用  1-启用',
  `OtherField1` varchar(50) default NULL COMMENT '自定义1',
  `OtherField2` varchar(50) default NULL COMMENT '自定义2',
  `OtherField3` varchar(50) default NULL COMMENT '自定义3',
  PRIMARY KEY  (`Id`),
  KEY `FK675951272AB6672C` (`CategoryId`),
  KEY `UnitId` (`UnitId`),
  CONSTRAINT `FK675951272AB6672C` FOREIGN KEY (`CategoryId`) REFERENCES `jsh_materialcategory` (`Id`),
  CONSTRAINT `jsh_material_ibfk_1` FOREIGN KEY (`UnitId`) REFERENCES `jsh_unit` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_materialcategory`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_materialcategory`;
CREATE TABLE `jsh_materialcategory` (
  `Id` bigint(20) NOT NULL auto_increment,
  `Name` varchar(50) default NULL COMMENT '名称',
  `CategoryLevel` smallint(6) default NULL COMMENT '等级',
  `ParentId` bigint(20) default NULL,
  PRIMARY KEY  (`Id`),
  KEY `FK3EE7F725237A77D8` (`ParentId`),
  CONSTRAINT `FK3EE7F725237A77D8` FOREIGN KEY (`ParentId`) REFERENCES `jsh_materialcategory` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_materialproperty`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_materialproperty`;
CREATE TABLE `jsh_materialproperty` (
  `id` bigint(20) NOT NULL auto_increment,
  `nativeName` varchar(50) default NULL COMMENT '原始名称',
  `enabled` bit(1) default NULL COMMENT '是否启用',
  `sort` varchar(10) default NULL COMMENT '排序',
  `anotherName` varchar(50) default NULL COMMENT '别名',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_person`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_person`;
CREATE TABLE `jsh_person` (
  `Id` bigint(20) NOT NULL auto_increment,
  `Type` varchar(20) default NULL COMMENT '类型',
  `Name` varchar(50) default NULL COMMENT '姓名',
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_role`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_role`;
CREATE TABLE `jsh_role` (
  `Id` bigint(20) NOT NULL auto_increment,
  `Name` varchar(50) default NULL,
  `type` varchar(50) default NULL COMMENT '类型',
  `value` varchar(200) default NULL COMMENT '值',
  `description` varchar(100) default NULL COMMENT '描述',
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_supplier`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_supplier`;
CREATE TABLE `jsh_supplier` (
  `id` bigint(20) NOT NULL auto_increment,
  `supplier` varchar(255) NOT NULL COMMENT '供应商名称',
  `contacts` varchar(100) default NULL COMMENT '联系人',
  `phonenum` varchar(30) default NULL COMMENT '联系电话',
  `email` varchar(50) default NULL COMMENT '电子邮箱',
  `description` varchar(500) default NULL,
  `isystem` tinyint(4) default NULL,
  `type` varchar(20) default NULL COMMENT '类型',
  `enabled` bit(1) default NULL COMMENT '启用',
  `AdvanceIn` double default '0',
  `BeginNeedGet` double default NULL COMMENT '期初应收',
  `BeginNeedPay` double default NULL COMMENT '期初应付',
  `AllNeedGet` double default NULL COMMENT '累计应收',
  `AllNeedPay` double default NULL COMMENT '累计应付',
  `fax` varchar(30) default NULL COMMENT '传真',
  `telephone` varchar(30) default NULL COMMENT '手机',
  `address` varchar(50) default NULL COMMENT '地址',
  `taxNum` varchar(50) default NULL COMMENT '纳税人识别号',
  `bankName` varchar(50) default NULL COMMENT '开户行',
  `accountNumber` varchar(50) default NULL COMMENT '账号',
  `taxRate` double default NULL COMMENT '税率',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_systemconfig`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_systemconfig`;
CREATE TABLE `jsh_systemconfig` (
  `id` bigint(20) NOT NULL auto_increment COMMENT '编号',
  `type` varchar(50) default NULL COMMENT '类型',
  `name` varchar(100) default NULL COMMENT '名称',
  `value` varchar(200) default NULL COMMENT '值',
  `description` varchar(100) default NULL COMMENT '描述',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统参数';

-- ----------------------------
--  Table structure for `jsh_unit`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_unit`;
CREATE TABLE `jsh_unit` (
  `id` bigint(20) NOT NULL auto_increment,
  `UName` varchar(50) default NULL COMMENT '名称，支持多单位',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_user`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_user`;
CREATE TABLE `jsh_user` (
  `id` bigint(20) NOT NULL auto_increment,
  `username` varchar(255) NOT NULL COMMENT '用户姓名--例如张三',
  `loginame` varchar(255) default NULL COMMENT '登录用户名--可能为空',
  `password` varchar(50) default NULL COMMENT '登陆密码',
  `position` varchar(200) default NULL COMMENT '职位',
  `department` varchar(255) default NULL COMMENT '所属部门',
  `email` varchar(100) default NULL COMMENT '电子邮箱',
  `phonenum` varchar(100) default NULL COMMENT '手机号码',
  `ismanager` tinyint(4) NOT NULL default '1' COMMENT '是否为管理者 0==管理者 1==员工',
  `isystem` tinyint(4) NOT NULL default '1' COMMENT '是否系统自带数据 ',
  `status` tinyint(4) default NULL COMMENT '用户状态',
  `description` varchar(500) default NULL COMMENT '用户描述信息',
  `remark` varchar(500) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_userbusiness`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_userbusiness`;
CREATE TABLE `jsh_userbusiness` (
  `Id` bigint(20) NOT NULL auto_increment,
  `Type` varchar(50) default NULL COMMENT '类别',
  `KeyId` varchar(50) default NULL COMMENT '主ID',
  `Value` varchar(10000) default NULL COMMENT '值',
  `BtnStr` varchar(2000) default NULL COMMENT '按钮权限',
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jsh_visitaccount`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_visitaccount`;
CREATE TABLE `jsh_visitaccount` (
  `Id` bigint(20) NOT NULL auto_increment,
  `ProjectId` bigint(20) NOT NULL,
  `LouHao` varchar(50) default NULL,
  `HuHao` varchar(50) default NULL,
  `HuiFang` varchar(50) default NULL,
  `LuoShi` varchar(50) default NULL,
  `Name` varchar(50) default NULL,
  `Tel` varchar(50) default NULL,
  `AddTime` datetime default NULL,
  PRIMARY KEY  (`Id`),
  KEY `FKFF4AAE822888F9A` (`ProjectId`),
  CONSTRAINT `FKFF4AAE822888F9A` FOREIGN KEY (`ProjectId`) REFERENCES `jsh_depot` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records 
-- ----------------------------
INSERT INTO `databasechangelog` VALUES ('201709282202','jishenghua','liquibase/jsh_erp/db.changelog-jsh_erp-1.0.xml','2017-09-28 23:34:07','1','EXECUTED','7:ca3a38c3a43ee96bf6c7bbf56123d1fc','sql','增加角色bbbb-测试',NULL,'3.1.1'), ('201709282227','jishenghua','liquibase/jsh_erp/db.changelog-jsh_erp-1.0.xml','2017-09-28 23:34:07','2','EXECUTED','7:fa335b3dcabb52f38c4300e35b7c0b4c','sql','删除角色bbbb-测试',NULL,'3.1.1'), ('201709282322','jishenghua','liquibase/jsh_erp/db.changelog-jsh_erp-1.0.xml','2017-09-29 22:39:46','3','EXECUTED','7:adeea7031bd16af361001ce7d93b1e1a','sql','新增系统配置表',NULL,'3.1.1'), ('201709292218','jishenghua','liquibase/jsh_erp/db.changelog-jsh_erp-1.0.xml','2017-09-29 22:39:46','4','EXECUTED','7:f7079f8d7b3fdb92fb6d319789ea9117','sql','新增系统参数数据-公司相关',NULL,'3.1.1'), ('201710122314','jishenghua','liquibase/jsh_erp/db.changelog-jsh_erp-1.0.xml','2017-10-18 22:39:27','5','EXECUTED','7:c0885501076d6473461f074cc68535e7','sql','新增商品属性-数据',NULL,'3.1.1'), ('201712102245','jishenghua','liquibase/jsh_erp/db.changelog-jsh_erp-1.0.xml','2017-12-10 22:51:30','6','EXECUTED','7:9b0df7eba9ad678b08fd435be32397b1','sql','更新账户表-是否默认列',NULL,'3.1.1');
INSERT INTO `databasechangeloglock` VALUES ('1','\0',NULL,NULL);
INSERT INTO `jsh_account` VALUES ('4','南通建行','652346523465234623','1200','215','建行账户','\0'), ('9','流动总账','65234624523452364','2000','393','现在账户',''), ('10','支付宝','123456789@qq.com','10000',NULL,'','\0'), ('11','微信','13000000000','10000',NULL,'','\0'), ('12','上海农行','65324345234523211','10000','0','','\0');
INSERT INTO `jsh_accounthead` VALUES ('57','收预付款','8','3',NULL,'1000',NULL,'2342134','2017-06-27 00:00:00',''), ('61','收预付款','9','3',NULL,'33',NULL,'SYF2017062901721','2017-06-29 00:00:00','aaaaaa'), ('67','收预付款','10','4',NULL,'2100',NULL,'SYF2017070222414','2017-07-02 00:00:00',''), ('70','支出','4','3','-60','-60','4','ZC20170703233735','2017-07-03 00:00:00',''), ('74','转账',NULL,'3','-100','-100','4','ZZ2017070323489','2017-07-03 00:00:00',''), ('77','收入','2','3','40','40','4','SR20170704222634','2017-07-04 00:00:00',''), ('78','收预付款','9','3',NULL,'200',NULL,'SYF201707050257','2017-07-05 00:00:00',''), ('79','收预付款','9','3',NULL,'100',NULL,'SYF20170705076','2017-07-05 00:00:00',''), ('82','收款','2','3','0','2.6',NULL,'SK20171008191440','2017-10-09 00:08:11',''), ('83','付款','1','4','0','-20',NULL,'FK20171008232825','2017-10-08 00:00:00',''), ('84','收入','2','4','0','21','10','SR20171009000300','2017-10-09 00:03:00',''), ('85','收入','2','3','22','22','11','SR20171009000637','2017-10-09 00:06:37',''), ('86','转账',NULL,'4','-22','-22','10','ZZ20171009000719','2017-10-09 00:07:19',''), ('87','付款','4','4','10','-33',NULL,'FK20171009000747','2017-10-09 00:07:47',''), ('88','收款','2','4','0','2.8',NULL,'SK20171024220754','2017-10-24 22:07:54',''), ('89','收款','2','4','0','11',NULL,'SK20171030232535','2017-10-30 23:25:35',''), ('90','收款','2','4','0','10',NULL,'SK20171119231440','2017-11-19 23:14:40','');
INSERT INTO `jsh_accountitem` VALUES ('58','57','9',NULL,'1000',''), ('62','61','4',NULL,'33',''), ('68','67','4',NULL,'2100',''), ('71','70',NULL,'11','60',''), ('75','74','9',NULL,'100',''), ('78','77',NULL,'14','40',''), ('79','78','9',NULL,'200',''), ('80','79','9',NULL,'100',''), ('83','82','10',NULL,'2.6',''), ('84','83','10',NULL,'-20',''), ('85','84',NULL,'13','21',''), ('86','85',NULL,'12','22','44'), ('87','86','11',NULL,'22',''), ('88','87','10',NULL,'-33',''), ('89','88','10',NULL,'2.8',''), ('90','89','11',NULL,'11',''), ('91','90','12',NULL,'10','');
INSERT INTO `jsh_app` VALUES ('3','00','系统管理','app','0000000004.png','','1240','600','','\0','\0','desk','198','',''), ('6','','个人信息','app','0000000005.png','../user/password.jsp','600','400','\0','\0','\0','dock','200','',''), ('7','01','基础数据','app','0000000006.png','','1240','600','','\0','\0','desk','120','',''), ('8','02','组装拆卸','app','0000000007.png','','1350','630','','\0','','desk','030','',''), ('22','03','报表查询','app','0000000022.png','','1240','600','','\0','\0','desk','115','',''), ('23','04','零售管理','app','resizeApi.png','','1350','630','','\0','','desk','025','',''), ('24','05','入库管理','app','buy.png','','1350','630','','\0','','desk','027','',''), ('25','06','出库管理','app','sale.png','','1350','630','','\0','','desk','028','',''), ('26','07','财务管理','app','money.png','','1350','630','','\0','\0','desk','035','','');
INSERT INTO `jsh_asset` VALUES ('1','27','weizhi','','0',NULL,'11','2016-10-22 00:00:00','2016-10-21 00:00:00','2016-11-03 00:00:00','1231241','123124123','2','','2016-10','2016-10-22 20:04:48','63','2016-10-22 20:04:48','63'), ('3','29','weizhi',NULL,'0',NULL,'11','2016-10-22 00:00:00','2016-10-21 00:00:00','2016-11-03 00:00:00','1231241','123124123','2',NULL,NULL,'2017-07-22 18:42:14',NULL,'2017-07-22 18:42:14',NULL);
INSERT INTO `jsh_assetcategory` VALUES ('14','递延资产','1','递延资产'), ('15','无形资产','1','无形资产'), ('16','长期投资','1','长期投资'), ('17','固定资产','1','固定资产'), ('18','流动资产','1','流动资产');
INSERT INTO `jsh_assetname` VALUES ('1','联想Y450','17','1','','1'), ('2','惠普打印机','15','1','','0'), ('12','乐萌水杯','16','1','','1'), ('13','机顶盒','17','1','机顶盒','0'), ('14','TCL电视','17','1','','1'), ('15','手机','17','1','','1'), ('16','硬盘','16','1','','0'), ('17','毛笔','17','1','','0'), ('18','杯子','17','1','','0'), ('19','建造师证书','15','1','','0'), ('20','算量软件','14','1','','1'), ('21','cad软件','15','1','','0'), ('22','办公桌','17','1','','0'), ('23','笔记本','17','1','笔记本','1'), ('24','打印机','17','1','打印机','0'), ('25','电脑','17','1','电脑','0'), ('26','电动车','16','1','电动车','0'), ('27','电源线','17','1','电源线','0'), ('28','电源线666','17','1','','0'), ('29','电源线777','17','1','','0'), ('30','电源线8','17','1','','0'), ('31','电源线9','17','1','','0');
INSERT INTO `jsh_depot` VALUES ('1','叠石桥店','地址222','33','22','0','2','上海33'), ('2','公司总部','地址12355','44','22.22','0','1','总部'), ('3','金沙店','地址666','31','4','0','3','苏州'), ('4','1268200294','',NULL,NULL,'1','1',''), ('5','1268787965',NULL,NULL,NULL,'1','3',''), ('6','1269520625',NULL,NULL,NULL,'1','2','');
INSERT INTO `jsh_depothead` VALUES ('7','入库','采购',NULL,'GHDD201708120002','GHDD201708120002','季圣华','2017-08-12 12:04:07','2017-08-12 12:03:23','1',NULL,'12','-30',NULL,'-36','现付','abcdefg','',NULL,NULL,'10','3.6','32.4','30','[\"10\",\"9\"]','[\"10\",\"20\"]','45',''), ('8','出库','销售',NULL,'XHDD201708120001','XHDD201708120001','季圣华','2017-08-12 18:10:14','2017-08-12 18:09:45','2',NULL,'11','17',NULL,'24','现付','','<7>,<6>',NULL,NULL,'22','5.28','18.72',NULL,NULL,NULL,NULL,'\0'), ('9','入库','采购',NULL,'GHDD201708120003','GHDD201708120003','季圣华','2017-08-12 21:01:09','2017-08-12 21:00:36','1',NULL,'11','-100',NULL,'-120','现付','','',NULL,NULL,'10','12','108',NULL,NULL,NULL,NULL,''), ('10','入库','采购',NULL,'GHDD201708120004','GHDD201708120004','季圣华','2017-08-12 21:10:42','2017-08-12 21:10:16','1',NULL,'4','-10',NULL,'-12','现付','','',NULL,NULL,'10','1.2','10.8',NULL,NULL,NULL,NULL,''), ('11','入库','采购',NULL,'GHDD201708120005','jshenghua001','季圣华','2017-08-12 22:07:44','2017-08-12 22:06:37','1',NULL,'12','-20',NULL,'-24','现付','','',NULL,NULL,'10','2.4','21.6',NULL,NULL,NULL,NULL,''), ('12','入库','采购',NULL,'GHDD201708120006','GHDD201708120006','季圣华','2017-08-12 22:17:11','2017-08-12 22:16:35','1',NULL,'11','-10',NULL,'-12','现付','','',NULL,NULL,'10','1.2','10.8','0','[\"undefined\"]','[\"undefined\"]',NULL,''), ('13','入库','采购',NULL,'GHDD201708120007','jishenghua3','季圣华','2017-08-12 22:17:52','2017-08-12 22:17:14','1',NULL,'4','-20',NULL,'-24','现付','','',NULL,NULL,'10','2.4','21.6',NULL,NULL,NULL,NULL,''), ('14','入库','采购',NULL,'GHDD201708120008','jishenghua004','季圣华','2017-08-12 22:19:37','2017-08-12 22:19:07','1',NULL,'11','-30',NULL,'-36','现付','','',NULL,NULL,'10','3.6','32.4','0','[\"undefined\"]','[\"undefined\"]',NULL,''), ('16','入库','采购',NULL,'GHDD201708120009','jishenghua005','季圣华','2017-08-12 22:26:23','2017-08-12 22:25:14','1',NULL,'10','-20',NULL,'-24','现付','','',NULL,NULL,'10','2.4','21.6','0','[\"undefined\"]','[\"undefined\"]',NULL,'\0'), ('17','入库','采购',NULL,'GHDD201708120010','GHDD201708120010','季圣华','2017-08-12 22:28:20','2017-08-12 22:28:02','1',NULL,'9','-30',NULL,'-36','现付','','',NULL,NULL,'10','3.6','32.4',NULL,'[\"undefined\"]','[\"undefined\"]',NULL,'\0'), ('18','入库','采购',NULL,'GHDD201708120011','GHDD201708120011','季圣华','2017-08-12 22:30:08','2017-08-12 22:29:48','1',NULL,'4','-20',NULL,'-24','现付','','',NULL,NULL,'10','2.4','21.6',NULL,NULL,NULL,NULL,'\0'), ('19','入库','采购',NULL,'GHDD201708120012','GHDD201708120012','季圣华','2017-08-12 22:30:57','2017-08-12 22:29:32','1',NULL,NULL,'-10',NULL,'-26.4','现付','','','[\"4\"]','[\"-10\"]','10','2.64','23.76','0','[\"undefined\"]','[\"undefined\"]',NULL,'\0'), ('20','入库','采购',NULL,'GHDD201708120013','GHDD201708120013','季圣华','2017-08-12 22:46:43','2017-08-12 22:45:55','1',NULL,'10','-23',NULL,'-36','现付','','',NULL,NULL,'20','7.2','28.8',NULL,NULL,NULL,NULL,''), ('21','入库','采购',NULL,'GHDD201708120014','GHDD201708120014','季圣华','2017-08-12 22:46:52','2017-08-12 22:45:59','1',NULL,'11','-20',NULL,'-26.4','现付','','',NULL,NULL,'10','2.64','23.76','0','[\"undefined\"]','[\"undefined\"]',NULL,'\0'), ('22','入库','采购',NULL,'GHDD201708120015','GHDD201708120015','季圣华','2017-08-12 23:49:32','2017-08-12 23:48:24','1',NULL,'11','-20',NULL,'-24','现付','','',NULL,NULL,'10','2.4','21.6',NULL,NULL,NULL,NULL,'\0'), ('23','入库','采购',NULL,'GHDD201708140001','GHDD201708140001','季圣华','2017-08-14 20:41:54','2017-08-14 20:40:49','1',NULL,'4','-300',NULL,'-360','现付','','',NULL,NULL,'10','36','324',NULL,NULL,NULL,NULL,'\0'), ('24','入库','采购',NULL,'GHDD201708150001','GHDD201708150001','季圣华','2017-08-15 21:36:25','2017-08-15 21:35:38','1',NULL,'11','-675',NULL,'-750','现付','','',NULL,NULL,'10','75','675',NULL,NULL,NULL,NULL,'\0'), ('25','入库','采购',NULL,'GHDD201708150002','GHDD201708150002','季圣华','2017-08-15 22:31:46','2017-08-15 22:29:47','1',NULL,NULL,'-33',NULL,'-75','现付','ababab','','[\"9\",\"10\"]','[\"-22\",\"-11\"]','10','7.5','67.5','22','[\"10\",\"8\"]','[\"11\",\"11\"]',NULL,'\0'), ('26','入库','采购',NULL,'GHDD201708160001','GHDD201708160001','季圣华','2017-08-16 23:50:35','2017-08-16 23:47:42','4',NULL,'9','-162',NULL,'-150','现付','','',NULL,NULL,'10','18','162',NULL,NULL,NULL,NULL,'\0'), ('27','入库','采购',NULL,'GHDD201708180001','GHDD201708180001','季圣华','2017-08-18 00:25:58','2017-08-18 00:25:43','1',NULL,'11','-74.25',NULL,'-75','现付','','',NULL,NULL,'10','8.25','74.25',NULL,NULL,NULL,NULL,'\0'), ('28','入库','采购',NULL,'GHDD201708270001','GHDD201708270001','季圣华','2017-08-27 23:10:44','2017-08-27 23:06:05','46',NULL,'10','-64.8',NULL,'-72','现付','','',NULL,NULL,'10','7.2','64.8','10','[\"10\"]','[\"10\"]',NULL,'\0'), ('29','出库','销售',NULL,'XSCK201708280001','XSCK201708280001','季圣华','2017-08-28 23:06:40','2017-08-28 23:05:11','2',NULL,'11','120.85',NULL,'130','现付','','<7>',NULL,NULL,'10','13.65','122.85','0','[\"undefined\"]','[\"undefined\"]',NULL,'\0'), ('30','入库','销售退货',NULL,'XSTH201708280001','XSTH201708280001','季圣华','2017-08-28 23:13:08','2017-08-28 23:12:48','2',NULL,'10','-48',NULL,'-48','现付','','<5>,<6>',NULL,NULL,'0','0','48','0','[\"undefined\"]','[\"undefined\"]',NULL,'\0'), ('31','出库','采购退货',NULL,'CGTH201708280001','CGTH201708280001','季圣华','2017-08-28 23:15:45','2017-08-28 23:15:21','1',NULL,'10','28.6',NULL,'26','现付','','',NULL,NULL,'0','0','28.6','12','[\"10\"]','[\"12\"]',NULL,'\0'), ('32','入库','其它',NULL,'QTRK201708280001','QTRK201708280001','季圣华','2017-08-28 23:17:55','2017-08-28 23:17:33','1',NULL,NULL,NULL,NULL,'12','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('33','出库','其它',NULL,'QTCK201708280001','QTCK201708280001','季圣华','2017-08-28 23:21:14','2017-08-28 23:20:36','2',NULL,NULL,NULL,NULL,'65','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('37','出库','调拨',NULL,'DBCK201708280002','DBCK201708280002','季圣华','2017-08-28 23:56:34','2017-08-28 23:56:10',NULL,NULL,NULL,NULL,NULL,'1.3','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('38','出库','调拨',NULL,'DBCK201708290001','DBCK201708290001','季圣华','2017-08-29 00:20:11','2017-08-29 00:19:58',NULL,NULL,NULL,NULL,NULL,'2.6','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('41','出库','零售',NULL,'LSCK201708290002','LSCK201708290002','季圣华','2017-08-29 23:29:39','2017-08-29 23:29:06','7',NULL,'10','42',NULL,'42','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('42','出库','零售',NULL,'LSCK201708290003','LSCK201708290003','季圣华','2017-08-29 23:35:12','2017-08-29 23:33:21','7',NULL,'11','11',NULL,'11','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('43','出库','零售',NULL,'LSCK201708290004','LSCK201708290004','季圣华','2017-08-29 23:39:44','2017-08-29 23:39:28','7',NULL,'9','12.1',NULL,'12.1','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('44','入库','零售退货',NULL,'LSTH201708290001','LSTH201708290001','季圣华','2017-08-29 23:48:43','2017-08-29 23:46:35','7',NULL,'10','-2.2',NULL,'-2.2','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('45','入库','零售退货',NULL,'LSTH201708290002','LSTH201708290002','季圣华','2017-08-29 23:51:55','2017-08-29 23:51:31','7',NULL,'12','-3.3',NULL,'-3.3','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,''), ('48','出库','零售',NULL,'LSCK201708310001','LSCK201708310001','季圣华','2017-08-31 00:30:31','2017-08-31 00:29:10','7',NULL,NULL,'12',NULL,'12','现付','','','[\"10\",\"11\"]','[\"15\",\"20\"]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('49','出库','零售',NULL,'LSCK201708310002','LSCK201708310002','季圣华','2017-08-31 00:57:40','2017-08-31 00:57:08','7',NULL,NULL,'12',NULL,'12','现付','','','[\"9\",\"11\"]','[\"22\",\"11\"]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('50','出库','零售',NULL,'LSCK201709030001','LSCK201709030001','季圣华','2017-09-03 12:51:50','2017-09-03 12:51:21','10',NULL,'10','22',NULL,'22','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,''), ('52','出库','零售',NULL,'LSCK201709040001','LSCK201709040001','季圣华','2017-09-04 21:32:49','2017-09-04 21:31:24','7',NULL,'11','24.2',NULL,'24.2','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('53','出库','零售',NULL,'LSCK201709040002','LSCK201709040002','季圣华','2017-09-04 21:34:02','2017-09-04 21:33:30','7',NULL,'9','36.3',NULL,'36.3','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,''), ('54','入库','采购',NULL,'CGRK201709040001','CGRK201709040001','季圣华','2017-09-04 22:20:12','2017-09-04 22:13:00','1',NULL,'10','-10.8',NULL,'-12','现付','','',NULL,NULL,'10','1.2','10.8','12','[\"9\"]','[\"12\"]',NULL,'\0'), ('57','入库','采购',NULL,'CGRK201709050001','CGRK201709050001','季圣华','2017-09-05 22:37:54','2017-09-05 22:37:31','1',NULL,'11','-182.52',NULL,'-182.4','现付','','',NULL,NULL,'0','0','182.52','0','[\"undefined\"]','[\"undefined\"]',NULL,'\0'), ('60','出库','礼品充值',NULL,'LPCZ201709050001','LPCZ201709050001','季圣华','2017-09-05 23:45:48','2017-09-05 23:42:17',NULL,NULL,NULL,NULL,NULL,'13','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('61','出库','礼品销售',NULL,'LPXS201709050001','LPXS201709050001','季圣华','2017-09-05 23:48:10','2017-09-05 23:46:04',NULL,NULL,NULL,NULL,NULL,'6.5','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('62','出库','礼品充值',NULL,'LPCZ201709050002','LPCZ201709050002','季圣华','2017-09-05 23:52:41','2017-09-05 23:51:26',NULL,NULL,NULL,NULL,NULL,'4','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('63','入库','采购',NULL,'CGRK201709170001','CGRK201709170001','季圣华','2017-09-17 21:45:14','2017-09-17 21:44:50','1',NULL,'10','-13.2',NULL,'-12','现付','','',NULL,NULL,'0','0','13.2',NULL,NULL,NULL,NULL,''), ('65','入库','采购',NULL,'CGRK201709170002','CGRK201709170002','季圣华','2017-09-17 21:47:07','2017-09-17 20:45:55','1',NULL,NULL,'-42',NULL,'-39','现付','','','[\"12\",\"9\"]','[\"-20\",\"-22\"]','0','0','42.9','0','[\"undefined\"]','[\"undefined\"]',NULL,'\0'), ('68','其它','组装单',NULL,'ZZD2017092000001','ZZD2017092000001','季圣华','2017-09-20 23:29:28','2017-09-20 23:29:13',NULL,NULL,NULL,NULL,NULL,'7','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('69','其它','拆卸单',NULL,'CSD2017092000001','CSD2017092000001','季圣华','2017-09-20 23:40:55','2017-09-20 23:40:41',NULL,NULL,NULL,NULL,NULL,'0','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('70','入库','采购',NULL,'CGRK201709210001','CGRK201709210001','季圣华','2017-09-21 22:37:20','2017-09-21 22:36:37','1',NULL,NULL,'-50',NULL,'-50','现付','','','[\"4\",\"9\"]','[\"-10\",\"-40\"]','0','0','50','0','[\"undefined\"]','[\"undefined\"]',NULL,'\0'), ('71','入库','销售退货',NULL,'XSTH201709210001','XSTH201709210001','季圣华','2017-09-21 22:39:00','2017-09-21 22:38:37','2',NULL,'11','-48',NULL,'-48','现付','','<6>,<7>',NULL,NULL,'0','0','48',NULL,NULL,NULL,NULL,'\0'), ('72','入库','其它',NULL,'QTRK201709210001','QTRK201709210001','季圣华','2017-09-21 22:39:26','2017-09-21 22:39:14','4',NULL,NULL,NULL,NULL,'24','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('73','出库','销售',NULL,'XSCK201709210001','XSCK201709210001','季圣华','2017-09-21 22:40:01','2017-09-21 22:39:44','2',NULL,'11','10',NULL,'10','现付','','<6>',NULL,NULL,'0','0','10','0','[\"undefined\"]','[\"undefined\"]',NULL,'\0'), ('74','出库','采购退货',NULL,'CGTH201709210001','CGTH201709210001','季圣华','2017-09-21 22:40:57','2017-09-21 22:40:38','4',NULL,'4','5',NULL,'5','现付','','',NULL,NULL,'0','0','5',NULL,NULL,NULL,NULL,'\0'), ('75','出库','其它',NULL,'QTCK201709210001','QTCK201709210001','季圣华','2017-09-21 22:41:15','2017-09-21 22:41:02','2',NULL,NULL,NULL,NULL,'13','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('76','出库','调拨',NULL,'DBCK201709210001','DBCK201709210001','季圣华','2017-09-21 22:41:36','2017-09-21 22:41:19',NULL,NULL,NULL,NULL,NULL,'10','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('77','出库','零售',NULL,'LSCK201709210001','LSCK201709210001','季圣华','2017-09-21 22:42:44','2017-09-21 22:42:21','7',NULL,'4','2.2',NULL,'2.2','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('78','入库','零售退货',NULL,'LSTH201709210001','LSTH201709210001','季圣华','2017-09-21 22:46:07','2017-09-21 22:45:49','7',NULL,'4','-2.2',NULL,'-2.2','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('79','入库','采购',NULL,'CGRK201709210002','CGRK201709210002','季圣华','2017-09-21 23:16:37','2017-09-21 23:16:21','1',NULL,'11','-23.76',NULL,'-21.6','现付','','',NULL,NULL,'0','0','23.76',NULL,NULL,NULL,NULL,'\0'), ('80','其它','组装单',NULL,'ZZD2017092100001','ZZD2017092100001','季圣华','2017-09-21 23:17:16','2017-09-21 23:16:59',NULL,NULL,NULL,NULL,NULL,'5','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,''), ('82','入库','采购',NULL,'CGRK201709220001','CGRK201709220001','季圣华','2017-09-22 23:06:01','2017-09-22 23:05:39','1',NULL,NULL,'-50',NULL,'-52','现付','','','[\"11\",\"9\"]','[\"-20\",\"-30\"]','10','5.72','51.48','5','[\"11\",\"10\"]','[\"2\",\"3\"]',NULL,'\0'), ('84','入库','采购',NULL,'CGRK201709220002','CGRK201709220002','季圣华','2017-09-22 23:22:02','2017-09-22 23:21:48','1',NULL,'10','-26.4',NULL,'-24','现付','','',NULL,NULL,'0','0','26.4',NULL,NULL,NULL,NULL,'\0'), ('85','入库','采购',NULL,'CGRK201709240001','CGRK201709240001','季圣华','2017-09-24 22:46:00','2017-09-24 22:44:35','4',NULL,NULL,'-85',NULL,'-75','现付','','','[\"10\",\"9\"]','[\"-20\",\"-65\"]','0','0','87.75',NULL,NULL,NULL,NULL,'\0'), ('87','出库','销售',NULL,'XSCK201709250001','XSCK201709250001','季圣华','2017-09-25 22:24:08','2017-09-25 22:23:47','2',NULL,'10','4',NULL,'4','现付','','<6>',NULL,NULL,'0','0','4',NULL,NULL,NULL,NULL,'\0'), ('88','出库','销售',NULL,'XSCK201709250002','XSCK201709250002','季圣华','2017-09-25 22:36:51','2017-09-25 22:35:09','2',NULL,'10','39.6',NULL,'40','现付','','<7>',NULL,NULL,'10','4.4','39.6',NULL,NULL,NULL,NULL,'\0'), ('89','入库','零售退货',NULL,'LSTH201709260001','LSTH201709260001','季圣华','2017-09-26 00:26:52','2017-09-26 00:26:19','7',NULL,'9','-18',NULL,'-18','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('90','出库','零售',NULL,'LSCK201709260001','LSCK201709260001','季圣华','2017-09-26 22:31:24','2017-09-26 22:29:50','7',NULL,NULL,'100',NULL,'100','现付','','','[\"10\",\"11\"]','[\"60\",\"40\"]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('93','出库','销售',NULL,'XSCK201710080001','XSCK201710080001','季圣华','2017-10-08 19:12:23','2017-10-08 19:11:44','2',NULL,'10','0',NULL,'2.6','现付','','<7>',NULL,NULL,'0','0','2.6',NULL,NULL,NULL,NULL,'\0'), ('94','出库','销售',NULL,'XSCK201710080002','XSCK201710080002','季圣华','2017-10-08 19:58:55','2017-10-08 19:58:27','5',NULL,'9','0',NULL,'8','现付','','<6>',NULL,NULL,'0','0','8','0','[\"undefined\"]','[\"undefined\"]',NULL,'\0'), ('95','入库','采购',NULL,'CGRK201710180001','CGRK201710180001','季圣华','2017-10-18 23:21:24','2017-10-18 23:21:12','1',NULL,'11','-2.86',NULL,'-2.6','现付','','',NULL,NULL,'0','0','2.86',NULL,NULL,NULL,NULL,'\0'), ('96','出库','销售',NULL,'XSCK201710240001','XSCK201710240001','季圣华','2017-10-24 22:04:06','2017-10-24 22:03:08','2',NULL,'9','0',NULL,'2.8','现付','','<7>',NULL,NULL,'10','0.28','2.52','0','[\"undefined\"]','[\"undefined\"]',NULL,'\0'), ('97','入库','采购',NULL,'CGRK201710290001','CGRK201710290001','季圣华','2017-10-29 23:30:47','2017-10-29 23:30:08','4',NULL,'10','0',NULL,'-200','现付','','',NULL,NULL,'0','0','234',NULL,NULL,NULL,NULL,'\0'), ('98','入库','采购',NULL,'CGRK201710290002','CGRK201710290002','季圣华','2017-10-29 23:32:07','2017-10-29 23:30:52','4',NULL,'10','0',NULL,'-300','现付','','',NULL,NULL,'0','0','351',NULL,NULL,NULL,NULL,''), ('99','入库','采购',NULL,'CGRK201710290003','CGRK201710290003','季圣华','2017-10-29 23:33:45','2017-10-29 23:32:11','4',NULL,'11','-10',NULL,'-720','现付','','',NULL,NULL,'0','0','842.4',NULL,NULL,NULL,NULL,'\0'), ('100','出库','礼品销售',NULL,'LPXS201711010001','LPXS201711010001','季圣华','2017-11-01 23:06:40','2017-11-01 23:06:13',NULL,NULL,NULL,'0',NULL,'1','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('101','出库','调拨',NULL,'DBCK201711020001','DBCK201711020001','季圣华','2017-11-02 22:51:17','2017-11-02 22:48:58',NULL,NULL,NULL,'0',NULL,'50','现付','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('102','出库','零售',NULL,'LSCK201711060001','LSCK201711060001','季圣华','2017-11-06 20:38:46','2017-11-06 20:38:01','7',NULL,NULL,'12',NULL,'12','现付','','','[\"9\",\"12\"]','[\"10\",\"2\"]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0'), ('103','入库','采购',NULL,'CGRK201711070001','CGRK201711070001','季圣华','2017-11-07 21:07:05','2017-11-07 21:06:53','1',NULL,'10','-26.4',NULL,'-24','现付','','',NULL,'','0','0','26.4',NULL,NULL,NULL,NULL,'\0'), ('104','入库','采购',NULL,'CGRK201711070002','CGRK201711070002','季圣华','2017-11-07 21:07:40','2017-11-07 21:07:08','4',NULL,NULL,'-11',NULL,'-10','现付','','','[\"9\",\"11\"]','[\"-10\",\"-1\"]','0','0','11.7',NULL,NULL,NULL,NULL,'\0'), ('105','出库','销售',NULL,'XSCK201711070001','XSCK201711070001','季圣华','2017-11-07 21:08:48','2017-11-07 21:08:34','2',NULL,'10','13',NULL,'13','现付','','<6>',NULL,'','0','0','13',NULL,NULL,NULL,NULL,'\0'), ('106','出库','销售',NULL,'XSCK201711070002','XSCK201711070002','季圣华','2017-11-07 21:09:20','2017-11-07 21:08:51','2',NULL,NULL,'13',NULL,'13','现付','','<5>','[\"9\",\"10\"]','[\"5\",\"8\"]','0','0','13',NULL,NULL,NULL,NULL,'\0'), ('107','入库','采购',NULL,'CGRK201712030001','CGRK201712030001','季圣华','2017-12-03 22:38:36','2017-12-03 22:37:26','4',NULL,'9','-1',NULL,'-1','现付','','',NULL,'','0','0','1','0','[\"undefined\"]','[\"undefined\"]',NULL,'\0'), ('108','入库','采购',NULL,'CGRK201712030002','sdfasdfa','季圣华','2017-12-03 22:40:57','2017-12-03 22:40:38','4',NULL,'4','-42.12',NULL,'-36','现付','','',NULL,'','0','0','42.12',NULL,'[\"undefined\"]','[\"undefined\"]',NULL,'\0'), ('109','入库','采购',NULL,'CGRK201712030003','CGRK201712030003','季圣华','2017-12-03 22:41:38','2017-12-03 22:41:01','4',NULL,'11','-1.4',NULL,'-1.2','现付','','',NULL,'','0','0','1.4',NULL,'[\"undefined\"]','[\"undefined\"]',NULL,'\0'), ('110','入库','采购',NULL,'CGRK201712050001','CGRK201712050001','季圣华','2017-12-05 23:05:48','2017-12-05 23:05:34','1',NULL,'10','-11',NULL,'-10','现付','','',NULL,'','0','0','11',NULL,NULL,NULL,NULL,'\0'), ('111','入库','采购',NULL,'CGRK201712050002','CGRK201712050002','季圣华','2017-12-05 23:12:53','2017-12-05 23:12:40','1',NULL,'10','0',NULL,'-20','现付','','',NULL,'','0','0','22','0','[\"undefined\"]','[\"undefined\"]',NULL,'\0'), ('112','出库','销售',NULL,'XSCK201712100001','XSCK201712100001','季圣华','2017-12-10 21:07:45','2017-12-10 21:07:25','2',NULL,'11','2.6',NULL,'2.6','现付','','<>',NULL,'','0','0','2.6',NULL,NULL,NULL,NULL,'\0'), ('113','入库','采购',NULL,'CGRK201712100001','CGRK201712100001','季圣华','2017-12-10 23:11:20','2017-12-10 23:11:10','4',NULL,'9','-1.52',NULL,'-1.3','现付','','',NULL,'','0','0','1.52',NULL,'[\"undefined\"]','[\"undefined\"]',NULL,'\0');
INSERT INTO `jsh_depotitem` VALUES ('7','7','500','码','30','30','1.2','1.32','36','remark',NULL,NULL,'3',NULL,'10','3.6','39.6','a','b','c','','',NULL), ('8','8','500','码','20','20','1.2','1.2','24','',NULL,NULL,'3',NULL,NULL,'0','24','','','','','',NULL), ('9','9','500','码','100','100','1.2','1.32','120','',NULL,NULL,'3',NULL,'10','12','132','','','','','',NULL), ('10','10','500','码','10','10','1.2','1.32','12','',NULL,NULL,'3',NULL,'10','1.2','13.2','','','','','',NULL), ('11','11','500','码','20','20','1.2','1.32','24','',NULL,NULL,'3',NULL,'10','2.4','26.4','','','','','',NULL), ('12','12','500','码','10','10','1.2','1.32','12','',NULL,NULL,'3',NULL,'10','1.2','13.2','','','','','',NULL), ('13','13','500','码','20','20','1.2','1.32','24','',NULL,NULL,'3',NULL,'10','2.4','26.4','','','','','',NULL), ('14','14','500','码','30','30','1.2','1.32','36','',NULL,NULL,'3',NULL,'10','3.6','39.6','','','','','',NULL), ('16','16','500','码','20','20','1.2','1.32','24','',NULL,NULL,'3',NULL,'10','2.4','26.4','','','','','',NULL), ('17','17','500','码','30','30','1.2','1.32','36','',NULL,NULL,'3',NULL,'10','3.6','39.6','','','','','',NULL), ('18','18','500','码','20','20','1.2','1.32','24','',NULL,NULL,'3',NULL,'10','2.4','26.4','','','','','',NULL), ('19','19','500','码','22','22','1.2','1.32','26.4','',NULL,NULL,'3',NULL,'10','2.64','29.04','','','','','',NULL), ('20','20','500','码','30','30','1.2','1.32','36','',NULL,NULL,'3',NULL,'10','3.6','39.6','','','','','',NULL), ('21','21','500','码','22','22','1.2','1.32','26.4','',NULL,NULL,'3',NULL,'10','2.64','29.04','','','','','',NULL), ('22','22','500','码','20','20','1.2','1.32','24','',NULL,NULL,'3',NULL,'10','2.4','26.4','','','','','',NULL), ('23','23','517','箱','10','120','36','36','360','',NULL,NULL,'3',NULL,'0','0','360','','','','','',NULL), ('24','24','518','包','10','250','75','75','750','',NULL,NULL,'3',NULL,'0','0','750','','','','','',NULL), ('25','25','518','包','1','25','75','75','75','',NULL,NULL,'3',NULL,'0','0','75','','','','','',NULL), ('26','26','518','包','2','50','75','90','150','',NULL,NULL,'3',NULL,'20','30','180','','','','','',NULL), ('27','27','518','包','1','25','75','82.5','75','',NULL,NULL,'3',NULL,'10','7.5','82.5','','','','','',NULL), ('28','28','517','箱','2','24','36','36','72','',NULL,NULL,'3',NULL,'0','0','72','','','','','',NULL), ('29','29','500','码','100','100','1.3','1.37','130','',NULL,NULL,'3',NULL,'5','6.5','136.5','','','','','',''), ('30','30','500','码','40','40','1.2','1.2','48','',NULL,NULL,'3',NULL,'0','0','48','','','','','',NULL), ('31','31','500','码','20','20','1.3','1.43','26','',NULL,NULL,'3',NULL,'10','2.6','28.6','','','','','',NULL), ('32','32','500','码','10','10','1.2','1.32','12','',NULL,NULL,'3',NULL,'10','1.2','13.2','','','','','',NULL), ('33','33','500','码','50','50','1.3','1.43','65','',NULL,NULL,'3',NULL,'10','6.5','71.5','','','','','',NULL), ('37','37','500','码','1','1','1.3','1.3','1.3','',NULL,NULL,'3','1','0','0','1.3','','','','','',NULL), ('38','38','500','码','2','2','1.3','1.3','2.6','',NULL,NULL,'3','1','0','0','2.6','','','','','',NULL), ('41','41','500','码','20','20','2.1','2.31','42','',NULL,NULL,'3',NULL,'10','4.2','46.2','','','','','',NULL), ('42','42','500','码','10','10','1.1','1.1','11','',NULL,NULL,'3',NULL,'0','0','11','','','','','',NULL), ('43','43','500','码','11','11','1.1','1.1','12.1','',NULL,NULL,'3',NULL,'0','0','12.1','','','','','',NULL), ('44','44','499','码','1','1','2.2','2.2','2.2','',NULL,NULL,'3',NULL,'0','0','2.2','','','','','',NULL), ('45','45','500','码','3','3','1.1','1.1','3.3','',NULL,NULL,'3',NULL,'0','0','3.3','','','','','',NULL), ('48','48','498','码','10','10','1.2','1.2','12','',NULL,NULL,'3',NULL,'0','0','12','','','','','',NULL), ('49','49','498','码','10','10','1.2','1.2','12','',NULL,NULL,'3',NULL,'0','0','12','','','','','',NULL), ('50','50','500','码','20','20','1.1','1.1','22','',NULL,NULL,'3',NULL,'0','0','22','','','','','',NULL), ('52','52','500','码','22','22','1.1','1.1','24.2','',NULL,NULL,'3',NULL,'0','0','24.2','','','','','',NULL), ('53','53','500','码','33','33','1.1','1.1','36.3','',NULL,NULL,'3',NULL,'0','0','36.3','','','','','',NULL), ('54','54','500','码','10','10','1.2','1.2','12','',NULL,NULL,'1',NULL,'10','0','12','','','','','',''), ('57','57','500','码','2','2','1.2','1.26','2.4','',NULL,NULL,'3',NULL,'0','0.12','2.52','','','','','',NULL), ('60','60','500','码','10','10','1.3','1.3','13','',NULL,NULL,'3','4','0','0','13','','','','','',NULL), ('61','61','500','码','5','5','1.3','1.3','6.5','',NULL,NULL,'4',NULL,'0','0','6.5','','','','','',NULL), ('62','62','517','瓶','1','1','4','4','4','',NULL,NULL,'1','6','0','0','4','','','','','',NULL), ('63','57','517','箱','5','60','36','36','180','',NULL,NULL,'3',NULL,'0','0','180','','','','','',NULL), ('64','63','500','码','10','10','1.2','1.32','12','',NULL,NULL,'3',NULL,'10','1.2','13.2','','','','','',NULL), ('66','65','498','码','30','30','1.3','1.43','39','',NULL,NULL,'3',NULL,'10','3.9','42.9','','','','','',NULL), ('71','68','498','码','1','1','3','3','3','',NULL,NULL,'3',NULL,'0','0','3','','','','','','组合件'), ('72','68','499','码','1','1','4','4','4','',NULL,NULL,'3',NULL,'0','0','4','','','','','','普通子件'), ('73','69','498','码','1','1','0','0','0','',NULL,NULL,'1',NULL,'0','0','0','','','','','','组合件'), ('74','69','499','码','1','1','0','0','0','',NULL,NULL,'1',NULL,'0','0','0','','','','','','普通子件'), ('75','70','487','码','50','50','1','1','50','',NULL,NULL,'1',NULL,'10','0','50','','','','','',''), ('76','71','499','码','20','20','2.4','2.4','48','',NULL,NULL,'3',NULL,'0','0','48','','','','','',''), ('77','72','499','码','10','10','2.4','2.81','24','',NULL,NULL,'3',NULL,'17','4.08','28.08','','','','','',''), ('78','73','487','码','10','10','1','1','10','',NULL,NULL,'1',NULL,'0','0','10','','','','','',''), ('79','74','487','码','5','5','1','1','5','',NULL,NULL,'3',NULL,'0','0','5','','','','','',''), ('80','75','500','码','10','10','1.3','1.3','13','',NULL,NULL,'3',NULL,'0','0','13','','','','','',''), ('81','76','487','码','10','10','1','1','10','',NULL,NULL,'3','1','0','0','10','','','','','',''), ('82','77','499','码','1','1','2.2','2.2','2.2','',NULL,NULL,'3',NULL,'0','0','2.2','','','','','',''), ('83','78','499','码','1','1','2.2','2.2','2.2','',NULL,NULL,'3',NULL,'0','0','2.2','','','','','',''), ('84','79','499','码','9','9','2.4','2.64','21.6','',NULL,NULL,'3',NULL,'10','2.16','23.76','','','','','',''), ('85','80','500','码','1','1','2','2','2','',NULL,NULL,'3',NULL,'0','0','2','','','','','','组合件'), ('86','80','498','码','1','1','3','3','3','',NULL,NULL,'3',NULL,'0','0','3','','','','','','普通子件'), ('88','82','498','码','40','40','1.3','1.43','52','',NULL,NULL,'3',NULL,'10','5.2','57.2','','','','','',''), ('90','84','499','码','10','10','2.4','2.64','24','',NULL,NULL,'1',NULL,'10','2.4','26.4','','','','','',''), ('91','85','518','包','1','25','75','87.75','75','',NULL,NULL,'3',NULL,'17','12.75','87.75','','','','','',''), ('93','87','518','kg','1','1','4','4','4','',NULL,NULL,'3',NULL,'0','0','4','','','','','',''), ('94','88','517','瓶','10','10','4','4.4','40','',NULL,NULL,'3',NULL,'10','4','44','','','','','',''), ('95','89','517','箱','1','12','18','18','18','',NULL,NULL,'3',NULL,'0','0','18','','','','','',''), ('96','90','518','包','2','50','50','50','100','',NULL,NULL,'3',NULL,'0','0','100','','','','','',''), ('100','93','500','码','2','2','1.3','1.3','2.6','',NULL,NULL,'1',NULL,'0','0','2.6','','','','','',''), ('101','94','517','瓶','2','2','4','4','8','',NULL,NULL,'3',NULL,'0','0','8','','','','','',''), ('102','95','498','码','2','2','1.3','1.43','2.6','',NULL,NULL,'3',NULL,'10','0.26','2.86','','','','','',''), ('103','96','498','码','2','2','1.4','1.4','2.8','',NULL,NULL,'3',NULL,'0','0','2.8','','','','','',''), ('104','97','485','码','200','200','1','1.17','200','',NULL,NULL,'3',NULL,'17','34','234','','','','','',''), ('105','98','487','码','300','300','1','1.17','300','',NULL,NULL,'3',NULL,'17','51','351','','','','','',''), ('106','99','517','箱','20','240','36','42.12','720','',NULL,NULL,'3',NULL,'17','122.4','842.4','','','','','',''), ('107','100','487','码','1','1','1','1','1','',NULL,NULL,'4',NULL,'0','0','1','','','','','',''), ('108','101','485','码','50','50','1','1','50','',NULL,NULL,'3','1','0','0','50','','','','','',''), ('109','102','498','码','10','10','1.2','1.2','12','',NULL,NULL,'3',NULL,'0','0','12','','','','','',''), ('110','103','499','码','10','10','2.4','2.64','24','',NULL,NULL,'3',NULL,'10','2.4','26.4','','','','','',''), ('111','104','487','码','10','10','1','1.17','10','',NULL,NULL,'3',NULL,'17','1.7','11.7','','','','','',''), ('112','105','499','码','5','5','2.6','2.6','13','',NULL,NULL,'3',NULL,'0','0','13','','','','','',''), ('113','106','499','码','5','5','2.6','2.6','13','',NULL,NULL,'3',NULL,'0','0','13','','','','','',''), ('114','107','487','码','1','1','1','1','1','',NULL,NULL,'1',NULL,'0','0','1','','','','','',''), ('116','108','517','箱','1','12','36','42.12','36','',NULL,NULL,'3',NULL,'17','6.12','42.12','','','','','',''), ('117','109','500','码','1','1','1.2','1.4','1.2','',NULL,NULL,'3',NULL,'17','0.2','1.4','','','','','',''), ('118','110','487','码','10','10','1','1.1','10','',NULL,NULL,'3',NULL,'10','1','11','','','','','',''), ('119','111','487','码','20','20','1','1.1','20','',NULL,NULL,'1',NULL,'10','2','22','','','','','',''), ('120','112','499','码','1','1','2.6','2.6','2.6','',NULL,NULL,'3',NULL,'0','0','2.6','','','','','',''), ('121','113','498','码','1','1','1.3','1.52','1.3','',NULL,NULL,'3',NULL,'17','0.22','1.52','','','','','','');
INSERT INTO `jsh_functions` VALUES ('1','00','系统管理','0','','','0010','','电脑版',''), ('2','01','基础数据','0','','','0020','','电脑版',''), ('11','0001','系统管理','00','','\0','0110','','电脑版',NULL), ('12','000101','应用管理','0001','../manage/app.jsp','\0','0132','','电脑版',''), ('13','000102','角色管理','0001','../manage/role.jsp','\0','0130','','电脑版',''), ('14','000103','用户管理','0001','../manage/user.jsp','\0','0140','','电脑版',NULL), ('15','000104','日志管理','0001','../manage/log.jsp','\0','0160','','电脑版',NULL), ('16','000105','功能管理','0001','../manage/functions.jsp','\0','0135','','电脑版',NULL), ('21','0101','商品管理','01','','\0','0220','','电脑版',NULL), ('22','010101','商品类别','0101','../materials/materialcategory.jsp','\0','0230','','电脑版',NULL), ('23','010102','商品信息','0101','../materials/material.jsp','\0','0240','','电脑版',NULL), ('24','0102','基本资料','01','','\0','0250','','电脑版',NULL), ('25','01020101','供应商信息','0102','../manage/vendor.jsp','\0','0260','','电脑版','1,2'), ('26','010202','仓库信息','0102','../manage/depot.jsp','\0','0270','','电脑版',NULL), ('31','010206','经手人管理','0102','../materials/person.jsp','\0','0284','','电脑版',NULL), ('32','0502','入库管理','05','','\0','0330','','电脑版',''), ('33','050201','采购入库','0502','../materials/purchase_in_list.jsp','\0','0340','','电脑版','3,4,5'), ('38','0603','出库管理','06','','\0','0390','','电脑版',''), ('40','060306','调拨出库','0603','../materials/allocation_out_list.jsp','\0','0420','','电脑版','3,4,5'), ('41','060303','销售出库','0603','../materials/sale_out_list.jsp','\0','0410','','电脑版','3,4,5'), ('44','0704','财务管理','07','','\0','0450','','电脑版',''), ('59','030101','库存状况','0301','../reports/in_out_stock_report.jsp','\0','0600','','电脑版',NULL), ('194','010204','收支项目','0102','../manage/inOutItem.jsp','\0','0282','','电脑版',NULL), ('195','010205','结算账户','0102','../manage/account.jsp','\0','0283','','电脑版',NULL), ('196','03','报表查询','0','','','0025','','电脑版',''), ('197','070402','收入单','0704','../financial/item_in.jsp','\0','0465','','电脑版',''), ('198','0301','报表查询','03','','\0','0570','','电脑版',NULL), ('199','060304','采购退货','0603','../materials/purchase_back_list.jsp','\0','0415','','电脑版','3,4,5'), ('200','050203','销售退货','0502','../materials/sale_back_list.jsp','\0','0350','','电脑版','3,4,5'), ('201','050204','其它入库','0502','../materials/other_in_list.jsp','\0','0360','','电脑版','3,4,5'), ('202','060305','其它出库','0603','../materials/other_out_list.jsp','\0','0418','','电脑版','3,4,5'), ('203','070403','支出单','0704','../financial/item_out.jsp','\0','0470','','电脑版',''), ('204','070404','收款单','0704','../financial/money_in.jsp','\0','0475','','电脑版',''), ('205','070405','付款单','0704','../financial/money_out.jsp','\0','0480','','电脑版',''), ('206','070406','转账单','0704','../financial/giro.jsp','\0','0490','','电脑版',''), ('207','030102','结算账户','0301','../reports/account_report.jsp','\0','0610','','电脑版',NULL), ('208','030103','进货统计','0301','../reports/buy_in_report.jsp','\0','0620','','电脑版',NULL), ('209','030104','销售统计','0301','../reports/sale_out_report.jsp','\0','0630','','电脑版',NULL), ('210','040102','零售出库','0401','../materials/retail_out_list.jsp','\0','0405','','电脑版','3,4,5'), ('211','040104','零售退货','0401','../materials/retail_back_list.jsp','\0','0407','','电脑版','3,4,5'), ('212','070407','收预付款','0704','../financial/advance_in.jsp','\0','0495','','电脑版',''), ('213','010207','礼品卡管理','0102','../manage/depotGift.jsp','\0','0290','','电脑版',''), ('214','040106','礼品充值','0401','../materials/gift_recharge_list.jsp','\0','0408','','电脑版','3,4,5'), ('215','040108','礼品销售','0401','../materials/gift_out_list.jsp','\0','0409','','电脑版','3,4,5'), ('216','030105','礼品卡统计','0301','../reports/gift_manage_report.jsp','\0','0635','','电脑版',''), ('217','01020102','客户信息','0102','../manage/customer.jsp','\0','0262','','电脑版','1,2'), ('218','01020103','会员信息','0102','../manage/member.jsp','\0','0263','','电脑版','1,2'), ('219','000107','资产管理','0001','../asset/asset.jsp','\0','0170','\0','电脑版',NULL), ('220','010103','计量单位','0101','../manage/unit.jsp','\0','0245','','电脑版',NULL), ('221','04','零售管理','0','','','0028','','电脑版',''), ('222','05','入库管理','0','','','0030','','电脑版',''), ('223','06','出库管理','0','','','0035','','电脑版',''), ('224','07','财务管理','0','','','0040','','电脑版',''), ('225','0401','零售管理','04','','\0','0401','','电脑版',''), ('226','030106','入库明细','0301','../reports/in_detail.jsp','\0','0640','','电脑版',''), ('227','030107','出库明细','0301','../reports/out_detail.jsp','\0','0645','','电脑版',''), ('228','030108','入库汇总','0301','../reports/in_material_count.jsp','\0','0650','','电脑版',''), ('229','030109','出库汇总','0301','../reports/out_material_count.jsp','\0','0655','','电脑版',''), ('230','02','组装拆卸','0','','','0022','','电脑版',''), ('231','0201','组装拆卸','02','','\0','0310','','电脑版',''), ('232','020101','组装单','0201','../materials/assemble_list.jsp','\0','0315','','电脑版','3,4,5'), ('233','020102','拆卸单','0201','../materials/disassemble_list.jsp','\0','0320','','电脑版','3,4,5'), ('234','000105','系统配置','0001','../manage/systemConfig.jsp','\0','0165','','电脑版',''), ('235','030110','客户对账','0301','../reports/customer_account.jsp','\0','0660','','电脑版',''), ('236','000106','商品属性','0001','../materials/materialProperty.jsp','\0','0168','','电脑版',''), ('237','030111','供应商对账','0301','../reports/vendor_account.jsp','\0','0665','','电脑版','');
INSERT INTO `jsh_inoutitem` VALUES ('1','办公耗材','支出','办公耗材'), ('5','房租收入','收入','房租收入'), ('7','利息收入','收入','利息收入'), ('8','水电费','支出','水电费水电费'), ('9','快递费','支出','快递费'), ('10','交通报销费','支出','交通报销费'), ('11','差旅费','支出','差旅费'), ('12','全车贴膜-普通','收入',''), ('13','全车贴膜-高档','收入',''), ('14','洗车','收入',''), ('15','保养汽车','收入','');
INSERT INTO `jsh_material` VALUES ('485','2','棉线','a1',NULL,'100','A21-4321','5g','白色','码','','1','1','1','1',NULL,'','','[{\"basic\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}},{\"other\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]','','b2','c3','d4'), ('487','1','网布','制造商b',NULL,'100','12343','10g','','码','','1','1','1','1',NULL,'','','[{\"basic\":{\"Unit\":\"kg\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}},{\"other\":{\"Unit\":\"包\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]','',NULL,NULL,NULL), ('498','1','蕾丝','制造商c',NULL,NULL,'B123a','6g','','码','','1.2','1','1.3','1.4',NULL,'','','[{\"basic\":{\"Unit\":\"kg\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}},{\"other\":{\"Unit\":\"包\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]','',NULL,NULL,NULL), ('499','1','棉线','制造商d',NULL,NULL,'A21-1234','7g','','码','','2.2','2','2.4','2.6',NULL,'','','[{\"basic\":{\"Unit\":\"kg\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}},{\"other\":{\"Unit\":\"包\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]','',NULL,NULL,NULL), ('500','1','纯棉线','制造商e',NULL,NULL,'AAA666','11g','','码','','1.1','1','1.2','1.3',NULL,'','','[{\"basic\":{\"Unit\":\"kg\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}},{\"other\":{\"Unit\":\"包\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]','',NULL,NULL,NULL), ('517','1','奶酪','制造商',NULL,NULL,'AAAA','12ml','','','',NULL,NULL,NULL,NULL,'8','瓶','箱','[{\"basic\":{\"Unit\":\"瓶\",\"RetailPrice\":\"1.5\",\"LowPrice\":\"2\",\"PresetPriceOne\":\"3\",\"PresetPriceTwo\":\"4\"}},{\"other\":{\"Unit\":\"箱\",\"RetailPrice\":\"18\",\"LowPrice\":\"24\",\"PresetPriceOne\":\"36\",\"PresetPriceTwo\":\"48\"}}]','',NULL,NULL,NULL), ('518','1','安慕希','伊利',NULL,NULL,'abcd','350ml','银白色','','',NULL,NULL,NULL,NULL,'2','kg','包','[{\"basic\":{\"Unit\":\"kg\",\"RetailPrice\":\"2\",\"LowPrice\":\"1\",\"PresetPriceOne\":\"3\",\"PresetPriceTwo\":\"4\"}},{\"other\":{\"Unit\":\"包\",\"RetailPrice\":\"50\",\"LowPrice\":\"25\",\"PresetPriceOne\":\"75\",\"PresetPriceTwo\":\"100\"}}]','','','',''), ('562','1','红苹果（蛇果）','',NULL,NULL,'60#','大铁筐','','','',NULL,NULL,NULL,NULL,NULL,'','','[{\"basic\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}},{\"other\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]','','','','');
INSERT INTO `jsh_materialcategory` VALUES ('1','根目录','1','1'), ('2','花边一级A','1','1'), ('3','花边一级B','1','1'), ('4','其他','2','3'), ('5','其他','3','4'), ('6','花边二级A','2','2'), ('7','花边三级A','3','6'), ('8','花边二级B','2','2'), ('9','花边一级C','1','1'), ('10','花边三级B','3','6');
INSERT INTO `jsh_materialproperty` VALUES ('1','规格','','02','规格'), ('2','颜色','','01','颜色'), ('3','制造商','\0','03','制造商'), ('4','自定义1','\0','04','自定义1'), ('5','自定义2','\0','05','自定义2'), ('6','自定义3','\0','06','自定义3');
INSERT INTO `jsh_person` VALUES ('3','财务员','王五-财务'), ('4','财务员','赵六-财务'), ('5','业务员','小李'), ('6','业务员','小军'), ('7','业务员','小曹');
INSERT INTO `jsh_role` VALUES ('4','管理员',NULL,NULL,NULL), ('5','仓管员',NULL,NULL,NULL), ('6','aaaa',NULL,NULL,NULL);
INSERT INTO `jsh_supplier` VALUES ('1','上海某某花边工厂','乔治','','','','1','供应商','','0',NULL,'20',NULL,NULL,'','','','','','','10'), ('2','客户AAAA','佩琪','','','','1','客户','','24','10',NULL,NULL,NULL,'','','','','','',NULL), ('4','苏州新源布料厂','龙哥','13000000000','312341@qq.com','55','1','供应商','','0',NULL,'44',NULL,NULL,'','','','','','','17'), ('5','客户BBBB','彪哥','13000000000','666@qq.com','','1','客户','','36','20',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL), ('6','南通宝贝家纺','姗姗','1231','31243@qq.com','备注备注备注','1','客户','','0','5',NULL,NULL,NULL,'2134','15678903','地址地址地址','纳税人识别号','开户行','31234124312','0.17'), ('7','非会员','宋江','13000000000','123456@qq.com','','1','会员','','76.6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL), ('8','hy00001','宋江','13000000000','','','1','会员','','956',NULL,NULL,NULL,NULL,'','','','','','',NULL), ('9','hy00002','吴用','13000000000','','','1','会员','','344',NULL,NULL,NULL,NULL,'','','','','','',NULL), ('10','1268787965','李逵','82567384','423@qq.com','','1','会员','','2122',NULL,NULL,NULL,NULL,'','13000000001','','','','',NULL), ('45','666666666','武松','82567384','423@qq.com','','1','会员','','2100','0','0','0','0','','13000000001','','','','','0'), ('46','南通居梦莱家纺','曹操','','','','1','供应商','','0',NULL,NULL,'0','0','','13000000000','','','','',NULL);
INSERT INTO `jsh_systemconfig` VALUES ('1','basic','company_name','南通jshERP公司','公司名称'), ('2','basic','company_contacts','张三','公司联系人'), ('3','basic','company_address','南通市通州区某某路','公司地址'), ('4','basic','company_tel','0513-10101010','公司电话'), ('5','basic','company_fax','0513-18181818','公司传真'), ('6','basic','company_post_code','226300','公司邮编');
INSERT INTO `jsh_unit` VALUES ('2','kg,包(1:25)'), ('8','瓶,箱(1:12)'), ('11','qwe,sed(1:33)');
INSERT INTO `jsh_user` VALUES ('63','季圣华','jsh','e10adc3949ba59abbe56e057f20f883e','','','','','0','1','-1','',NULL), ('64','张三','zs','e10adc3949ba59abbe56e057f20f883e','','销售','','','0','1',NULL,'',NULL), ('65','李四','ls','e10adc3949ba59abbe56e057f20f883e','','销售','','','0','1',NULL,'',NULL);
INSERT INTO `jsh_userbusiness` VALUES ('1','RoleAPP','4','[23][24][25][8][26][22][7][3][6]',NULL), ('2','RoleAPP','5','[8][7][6]',NULL), ('3','RoleAPP','6','[21][1][8]',NULL), ('4','RoleAPP','7','[21][1][8][11]',NULL), ('5','RoleFunctions','4','[13][12][16][14][15][234][236][22][23][220][25][217][218][26][194][195][31][213][232][233][59][207][208][209][216][226][227][228][229][235][237][210][211][214][215][33][200][201][41][199][202][40][197][203][204][205][206][212]','[{\"funId\":\"25\",\"btnStr\":\"1\"},{\"funId\":\"217\",\"btnStr\":\"1\"},{\"funId\":\"218\",\"btnStr\":\"1\"},{\"funId\":\"232\",\"btnStr\":\"3\"},{\"funId\":\"233\",\"btnStr\":\"3\"},{\"funId\":\"33\",\"btnStr\":\"3\"},{\"funId\":\"200\",\"btnStr\":\"3\"},{\"funId\":\"201\",\"btnStr\":\"3\"},{\"funId\":\"210\",\"btnStr\":\"3\"},{\"funId\":\"211\",\"btnStr\":\"3\"},{\"funId\":\"214\",\"btnStr\":\"3\"},{\"funId\":\"215\",\"btnStr\":\"3\"},{\"funId\":\"41\",\"btnStr\":\"3\"},{\"funId\":\"199\",\"btnStr\":\"3\"},{\"funId\":\"202\",\"btnStr\":\"3\"},{\"funId\":\"40\",\"btnStr\":\"3\"}]'), ('6','RoleFunctions','5','[22][23][25][26][194][195][31][33][200][201][41][199][202]',NULL), ('7','RoleFunctions','6','[13][12][16][33]','[{\"funId\":\"33\",\"btnStr\":\"4\"}]'), ('8','RoleAPP','8','[21][1][8][11][10]',NULL), ('9','RoleFunctions','7','[168][13][12][16][14][15][189][18][19][132]',NULL), ('10','RoleFunctions','8','[168][13][12][16][14][15][189][18][19][132][22][23][25][26][27][157][158][155][156][125][31][127][126][128][33][34][35][36][37][39][40][41][42][43][46][47][48][49][50][51][52][53][54][55][56][57][192][59][60][61][62][63][65][66][68][69][70][71][73][74][76][77][79][191][81][82][83][85][89][161][86][176][165][160][28][134][91][92][29][94][95][97][104][99][100][101][102][105][107][108][110][111][113][114][116][117][118][120][121][131][135][123][122][20][130][146][147][138][148][149][153][140][145][184][152][143][170][171][169][166][167][163][164][172][173][179][178][181][182][183][186][187]',NULL), ('11','RoleFunctions','9','[168][13][12][16][14][15][189][18][19][132][22][23][25][26][27][157][158][155][156][125][31][127][126][128][33][34][35][36][37][39][40][41][42][43][46][47][48][49][50][51][52][53][54][55][56][57][192][59][60][61][62][63][65][66][68][69][70][71][73][74][76][77][79][191][81][82][83][85][89][161][86][176][165][160][28][134][91][92][29][94][95][97][104][99][100][101][102][105][107][108][110][111][113][114][116][117][118][120][121][131][135][123][122][20][130][146][147][138][148][149][153][140][145][184][152][143][170][171][169][166][167][163][164][172][173][179][178][181][182][183][186][187][188]',NULL), ('12','UserRole','1','[5]',NULL), ('13','UserRole','2','[6][7]',NULL), ('14','UserDepot','2','[1][2][6][7]',NULL), ('15','UserDepot','1','[1][2][5][6][7][10][12][14][15][17]',NULL), ('16','UserRole','63','[4]',NULL), ('17','RoleFunctions','13','[46][47][48][49]',NULL), ('18','UserDepot','63','[1][3]',NULL), ('19','UserDepot','5','[6][45][46][50]',NULL), ('20','UserRole','5','[5]',NULL), ('21','UserRole','64','[5]',NULL), ('22','UserDepot','64','[1]',NULL), ('23','UserRole','65','[5]',NULL), ('24','UserDepot','65','[1]',NULL), ('25','UserCustomer','64','[5][2]',NULL), ('26','UserCustomer','65','[6]',NULL), ('27','UserCustomer','63','[5][2]',NULL);
